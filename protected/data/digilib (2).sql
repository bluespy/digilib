-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 02, 2012 at 04:37 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `digilib`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_buku`
--

CREATE TABLE IF NOT EXISTS `tbl_buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pengarang` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `penerbit` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `tanggal` date NOT NULL,
  `download` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=82 ;

--
-- Dumping data for table `tbl_buku`
--

INSERT INTO `tbl_buku` (`id`, `judul`, `pengarang`, `penerbit`, `tahun`, `isi`, `tags`, `keyword`, `status`, `tanggal`, `download`, `view`) VALUES
(4, 'yang lainnya', 'hendar', 'hendar', 2010, 'banyak', 'Teknik Informatika', 'baru', 2, '0000-00-00', NULL, 66),
(12, 'kj', '', '', NULL, '<p>j</p>', 'h', '', 2, '0000-00-00', NULL, NULL),
(13, 'kg', '', '', NULL, '<p>k</p>', 'f', '', 2, '0000-00-00', NULL, 2),
(14, 'kg', '', '', NULL, '<p>b</p>', 'f', '', 2, '0000-00-00', NULL, NULL),
(15, 'kn', '', '', NULL, '<p>m</p>', 'n', '', 2, '0000-00-00', NULL, NULL),
(16, 's', '', '', NULL, '<p>v</p>', 'z', '', 2, '0000-00-00', NULL, NULL),
(17, 's', '', '', NULL, '<p>v</p>', 'z', '', 2, '0000-00-00', NULL, NULL),
(42, 'bb', '', '', NULL, '<p>c</p>', 'v', '', 1, '0000-00-00', 1, NULL),
(43, 'sdf', '', '', NULL, '<p>zs</p>', 'c', '', 1, '0000-00-00', NULL, NULL),
(44, 'acd', '', '', NULL, '<p>a</p>', 'd', '', 1, '0000-00-00', NULL, NULL),
(45, 'acd', '', '', NULL, '<p>a</p>', 'd', '', 1, '0000-00-00', NULL, NULL),
(46, 'asp.net', '', '', NULL, '<p>a</p>', 'd', 'asp.net', 1, '0000-00-00', NULL, 1),
(54, 'Pintar database Mysql', 'hendar', 'hendar', 2001, '<p>MySQL, SQL server dll</p>', 'Teknik Informatika', 'MySQL', 2, '0000-00-00', NULL, 27),
(51, 'VB.net dan MySQL', 'suhendar', 'hendar', 2011, '<p>Banyak perusahaan manufaktur kendaraan menghabiskan biaya jutaan sampai milyaran rupiah untuk menguji dan mengevaluasi collision performance dari kendaraan. Test seperti ini merupakan test wajib yang dilakukan oleh perusahaan manufaktur kendaraan sehingga sering disebut �costly process of trial and error�. Terdapat tiga metode collision test yang dipakai perusahaan manufaktur kendaraan pada saat ini, yaitu Full-Wrap Frontal Collision Test, Offset Frontal Collision Test dan Side Collision Test. Proses simulasi komputer untuk mempelajari dan menganalisa kendaraan adalah salah satu cara mengeveluasi collision performance. Dan pada kesempatan ini, penulis mencoba mensimulasikan metode collision test ini ke dalam bentuk bahasa pemograman Ansys 8.0. Dengan memasukkan kecepatan yang bervariasi (sesuai standar SAE) dan dengan menggunakan metode Full-Wrap Frontal Collision Test dan Offset Frontal Collision Test didapatkan displacement dari steering coloumn pada kendaraan tidak lebih dari 127 mm.\r\n</p>', 'Teknik Informatika', 'MySQL, VB.net', 2, '0000-00-00', NULL, 80),
(53, 'Kalkulus Lanjut', 'hendar', 'hendar', 2001, '<p>umum</p>', 'Umum', 'Umum', 2, '0000-00-00', NULL, 9),
(55, 'Fisika Dasar', 'bamboomedia', 'bamboomedia', 2011, '<p>bamboomedia</p>', 'Sertifikasi', 'Umum', 2, '0000-00-00', NULL, 7),
(56, 'Web Dinamis Dengan PHP dan MySQL', 'hendar', 'matahari', 2012, '<span style="color: rgb(51, 51, 51); font-family: ''Times New Roman'', Times, serif; font-size: 12px; text-align: justify; background-color: rgb(255, 255, 255); ">Banyak perusahaan manufaktur kendaraan menghabiskan biaya jutaan sampai milyaran rupiah untuk menguji dan mengevaluasi collision performance dari kendaraan. Test seperti ini merupakan test wajib yang dilakukan oleh perusahaan manufaktur kendaraan sehingga sering disebut �costly process of trial and error�. Terdapat tiga metode collision test yang dipakai perusahaan manufaktur kendaraan pada saat ini, yaitu Full-Wrap Frontal Collision Test, Offset Frontal Collision Test dan Side Collision Test. Proses simulasi komputer untuk mempelajari dan menganalisa kendaraan adalah salah satu cara mengeveluasi collision performance. Dan pada kesempatan ini, penulis mencoba mensimulasikan metode collision test ini ke dalam bentuk bahasa pemograman Ansys 8.0. Dengan memasukkan kecepatan yang bervariasi (sesuai standar SAE) dan dengan menggunakan metode Full-Wrap Frontal Collision Test dan Offset Frontal Collision Test didapatkan displacement dari steering coloumn pada kendaraan tidak lebih dari 127 mm.</span>\r\n', 'Teknik Informatika, Teknik Mesin', 'PHP, MySQL', 2, '0000-00-00', NULL, 109),
(57, 'Jaringan Komputer', 'baru', 'baru', 2012, 'baru', 'Teknik Informatika', 'baru', 2, '0000-00-00', NULL, 46),
(58, 'Mencoba Memahami yii Framework', 'yii', 'yii', 2011, 'yii framework<br>', 'Teknik Informatika', 'yii', 2, '2012-03-12', NULL, 18),
(59, 'Struktur Diskrit', 'hendar', 'gramedia', 2010, 'banyak', 'Teknik Informatika', 'test', 2, '2012-03-16', NULL, 5),
(60, 'C#.net dan SQL SERVER', 'momo', 'Loko', 2011, 'Web doang<br>', 'Web, Teknik Informatika', 'Web', 1, '2012-03-24', NULL, 4),
(61, 'pandir', 'pandir', 'pandir', 2011, 'pandir', 'Umum', 'Umum', 1, '2012-03-24', NULL, 2),
(62, 'test', 'test', 'test', 2011, 'test', 'Umum', 'umum', 2, '2012-03-24', NULL, 3),
(63, 'test', 'test', 'test', 2011, 'test', 'Umum', 'Umum', 2, '2012-03-24', NULL, 1),
(64, 'Agile Web Application Development with Yii 1.1 and PHP5', 'Jeffery Winesett', 'Packt Publishing', 2010, 'Yii is a high-performance, component-based application development framework written in PHP. It helps ease the complexity of building large-scale applications. It enables maximum reusability in web programming, and can significantly accelerate the development process. It does so by allowing the developer to build on top of already well-written, well-tested, and production-ready code. It prevents you<br>from having to rewrite core functionality that is common across many of today''s web-based applications, allowing you to concentrate on the business rules and<br>logic specific to the unique application being built.<br>This book takes a very pragmatic approach to learning the Yii Framework. Throughout the chapters we introduce the reader to many of the core features of Yii by taking a test-first approach to building a real-world task tracking and issue management application called TrackStar. All of the code is provided. The reader should be able to borrow from all of the examples provided to get up and running quickly, but will also be exposed to deeper discussion and explanation to fully understand what is happening behind the scenes.', 'Teknik Informatika', 'PHP, YII, Framework, Web', 2, '2012-04-27', NULL, 25),
(65, 'A guide to project management', 'Frank Heyworth', 'COUNCIL OF EUROPE, CONSEIL DE L’EUROPE', 2002, '<div style="text-align: justify;">This guide has been produced to offer assistance to those</div><div style="text-align: justify;">responsible for, or involved in, educational projects related</div><div style="text-align: justify;">to language education. It is hoped that it will be of help</div><div style="text-align: justify;">specifically to participants in ECML workshops and other</div><div style="text-align: justify;">activities, but also on a much wider scale to those undertaking</div><div style="text-align: justify;">projects in other contexts. It is the result of a preparatory</div><div style="text-align: justify;">study and a workshop in Graz on “innovatory approaches to</div><div style="text-align: justify;">language education” held in November 1999 and two regional</div><div style="text-align: justify;">workshops held in Budapest and Liechtenstein in 2000. Many</div><div style="text-align: justify;">of the ideas in it come from the facilitators and participants</div><div style="text-align: justify;">in these workshops.</div>', 'Teknik Sipil', 'ECML, Workshop', 2, '2012-05-14', NULL, 1),
(66, 'Construction Project Management Handbook', 'Kam Shadan, P.E., ', 'FTA', 2007, '<div>The Federal Transit Administration (FTA) sponsored and developed the Construction Project Management Handbook to provide guidelines to</div><div>public transit agencies undertaking substantial construction projects either for the first time or with little experience in construction</div><div>management. Gannett Fleming, Inc., a national engineering and construction firm, developed this Handbook under contract to and with</div><div>guidance from the FTA Office of Technology. The project managers consisted of Henry Nejako, FTA Program Management Officer; and Kam</div><div>Shadan, P.E., Author, and Project Manager, Gannett Fleming, Inc.</div><div>This Handbooks provides comprehensive coverage of construction project management, including the applicability of the principles of project</div><div>management and of all phases of project development in sequence and in separate chapters—from project initiation through planning,</div><div>environmental clearance, real estate acquisition, design, construction, commissioning, and closeout. The Handbook will be of use to transit</div><div>agencies and their consultants, the FTA Regional Offices, and others responsible for the management of capital projects involving</div><div>construction of transit facilities or systems. The study is organized to provide the transit agency and the project manager with a clearer</div><div>understanding of the applicability of the structures and principles of construction project management.</div>', 'Teknik Sipil', 'Management', 2, '2012-05-14', NULL, 3),
(67, 'Effective Software Project Management', 'Robert K. Wysocki', 'Wiley Publishing inc', 2006, '<div>The Declaration of Interdependence (that Bob Wysocki, I, and others co-authored)</div><div>documents the fundamental principles that underlie an agile-adaptive approach to</div><div>project management. (See www.apln.org for the complete Declaration.) Two of</div><div>these principles, are particularly relevant to this book:</div><div>■■ We improve effectiveness and reliability through situationally specific</div><div>strategies, processes, and practices.</div><div>■■ We expect uncertainty and manage for it through iterations, anticipation,</div><div>and adaptation.</div><div>No two people are alike. No two teams are alike. No two projects are alike. Yet</div><div>many organizations and project managers attempt to “standardize” projects,</div><div>essentially trying again and again and again to pound square pegs into round</div><div>holes. I’ve watched team after team attack high-risk, high-uncertainty projects</div><div>with meticulously laid out plans that were complete and utter fantasy. Furthermore,</div><div>most team members knew that the plan was fantasy, but if you have</div><div>only square pegs, you use square pegs.</div><div>Bob introduces us to square, round, triangular, and polygonal pegs—just the</div><div>right one for specific situations. But even better, he helps us figure what kinds</div><div>of holes we have. It’s one thing to have a principle that says “situationally specific,”</div><div>but what are the situations? How many do we have? What are the key</div><div>characteristics that define a “situation” for a project manager? Bob introduces</div><div>us to a simple but powerful concept to guide practitioners in defining holes</div><div>(the situation) and then presents us with a suite of pegs (solutions) that fit each</div><div>type of hole.</div><div>Bob defines project situations using a four-quadrant analysis of the certainty, or</div><div>uncertainty, of both ends and means. With some projects the ends, the business</div><div>objectives and specific software requirements that enable us to meet the objectives,</div><div>are fairly well known. On others, they are ill defined in the beginning and</div><div>have to evolve over the life of the project as more is learned. Some projects may</div><div>utilize a well-understood and proven technology, while others employ bleeding</div><div>edge, state-of-the-art technology. When square peg project managers meet</div><div>uncertainty, they try to pound out that uncertainty with a detail plan—but in</div><div>reality that meticulous plan is nothing more than a superstition about the future.</div><div><div>However, when the objectives, requirements, and technology are well known,</div><div>we should be able to plan the project with some assurances that we can meet the</div><div>plans for scope, schedule, and cost.</div><div>To differentiate projects using a slightly different analogy, when both ends and</div><div>means are well known, we can utilize a traditional Plan-Do strategy in which</div><div>we lay out the plan and then execute the steps. When both ends and means are</div><div>not well known, the strategy could better be described as Envision-Explore.</div><div>We lay out a rough plan, but we assume that significant changes will occur as</div><div>we learn more during the project. The problem that many square-peg project</div><div>managers fail to grasp is that many, if not most, high-risk and high-uncertainty</div><div>issues cannot be “planned” away—they can only be “executed” away. You</div><div>have to experiment with different options in order to attack uncertainty.</div><div>So the certainty-uncertainty of both ends and means provides us with a framework</div><div>for identifying holes—specific situations. Bob next turns to the pegs—</div><div>strategies that fit certain problems—strategy options that are absolutely critical</div><div>in managing the variety of projects that organizations undertake today.</div><div>It is important to recognize that strategies and practices are separate things.</div><div>Some people incorrectly think a particular practice is “agile,” while another is</div><div>“traditional.” However, good practices can be used in either a traditional or an</div><div>agile project (daily team meetings, for example). The critical factor in project</div><div>management is strategy—the specific model of delivery one chooses to utilize.</div><div>Here again Bob elevates us from the simplistic—traditional or agile solutions—</div><div>to a wider, richer strategy selection. He identifies four uniquely different</div><div>strategies—Linear &amp; Incremental, Iterative, Adaptive, and Extreme—and then</div><div>provides us with the characteristics, advantages, and weaknesses of each.</div><div>In particular, Bob spends the bulk of the book delving into the latter three</div><div>strategies—Iterative, Adaptive, and Extreme—because as the Declaration of</div><div>Interdependence principle states, “We expect uncertainty and manage for it</div><div>through iterations, anticipation, and adaptation.” Today, when more and more</div><div>projects occupy the uncertainty of ends and means category (and the highest</div><div>valued ones also), newer Adaptive and Extreme strategies are needed. Bob not</div><div>only identifies these strategies, but defines them in enough detail that practitioners</div><div>can effectively utilize them.</div><div>If you are tired of trying to stuff square pegs in round holes, if you are having</div><div>trouble with projects where uncertainty and high risk create floundering projects,</div><div>then this is the book you need to read.</div><div>Jim Highsmith</div><div>Flagstaff, Arizona</div><div>November 2005</div></div>', 'Teknik Sipil', '-', 2, '2012-05-14', NULL, 1),
(68, 'Fundamentals of Project Management', 'JAMES P. LEWIS', 'AMACOM', 2007, '<div>“PMI” and the PMI logo are service and trademarks of the Project Management</div><div>Institute, Inc. which are registered in the United States of America and other nations;</div><div>“PMP” and the PMP logo are certification marks of the Project Management Institute,</div><div>Inc. which are registered in the United States of America and other nations; “PMBOK”,</div><div>“PM Network”, and “PMI Today” are trademarks of the Project Management Institute,</div><div>Inc. which are registered in the United States of America and other nations; “. . . building</div><div>professionalism in project management . . .” is a trade and service mark of the</div><div>Project Management Institute, Inc. which is registered in the United States of America</div><div>and other nations; and the Project Management Journal logo is a trademark of the</div><div>Project Management Institute, Inc.</div><div>Various names used by companies to distinguish their software and other products can</div><div>be claimed as trademarks. AMACOM uses such names throughout this book for editorial</div><div>purposes only, with no inflection of trademark violation. All such software or product</div><div>names are in initial capital letters of ALL CAPITAL letters. Individual companies</div><div>should be contacted for complete information regarding trademarks and registration.</div>', 'Teknik Sipil', '-', 2, '2012-05-14', NULL, 1),
(69, 'Practical Project Management Tips, Tactics, and Tools', 'Harvey A. Levine', 'JOHN WILEY & SONS, INC.', 2002, '<div>No part of this publication may be reproduced, stored in a retrieval system or transmitted</div><div>in any form or by any means, electronic, mechanical, photocopying, recording, scanning</div><div>or otherwise, except as permitted under Sections 107 or 108 of the 1976 United States</div><div>Copyright Act, without either the prior written permission of the Publisher, or</div><div>authorization through payment of the appropriate per-copy fee to the Copyright</div><div>Clearance Center, 222 Rosewood Drive, Danvers, MA 01923, (978) 750-8400,</div><div>fax (978) 750-4744. Requests to the Publisher for permission should be addressed to the</div><div>Permissions Department, John Wiley &amp; Sons, Inc., 605 Third Avenue, New York, NY</div><div>10158-0012, (212) 850-6011, fax (212) 850-6008, E-Mail: PERMREQ@WILEY.COM.</div><div>This publication is designed to provide accurate and authoritative information in regard to</div><div>the subject matter covered. It is sold with the understanding that the publisher is not</div><div>engaged in rendering professional services. If professional advice or other expert</div><div>assistance is required, the services of a competent professional person should be sought.</div><div>Wiley also publishes its books in a variety of electronic formats. Some content that</div><div>appears in print may not appear in electronic formats.</div><div>Designations used by companies to distinguish their products are often claimed as</div><div>trademarks. In all instances where John Wiley &amp; Sons, Inc. is aware of a claim, the</div><div>product names appear in initial capital or all capital letters. Readers, however, should</div><div>contact the appropriate companies for more complete information regarding trademarks</div><div>and registration.</div>', 'Teknik Sipil', '-', 2, '2012-05-14', NULL, 1),
(70, 'PROJECT MANAGEMENT A SYSTEMS APPROACH TO PLANNING, SCHEDULING, AND CONTROL L ING', 'HAROLD KERZNER, PH.D.', 'John Wiley & Sons, Inc.', 2009, '<div>No part of this publication may be reproduced, stored in a retrieval system or transmitted in any</div><div>form or by any means, electronic, mechanical, photocopying, recording, scanning or otherwise,</div><div>except as permitted under Section 107 or 108 of the 1976 United States Copyright Act, without</div><div>either the prior written permission of the Publisher, or authorization through payment of the</div><div>appropriate per-copy fee to the Copyright Clearance Center, 222 Rosewood Drive, Danvers, MA</div><div>01923, (978) 750-8400, fax (978) 646-8600, or on the web at www.copyright.com. Requests to the</div><div>Publisher for permission should be addressed to the Permissions Department, John Wiley &amp; Sons, Inc.,</div><div>111 River Street, Hoboken, NJ 07030, (201) 748-6011, fax (201) 748-6008, or online at</div><div>http://www.wiley.com/go/permission.</div><div>Limit of Liability/Disclaimer of Warranty: While the publisher and author have used their best efforts</div><div>in preparing this book, they make no representations or warranties with respect to the accuracy or</div><div>completeness of the contents of this book and specifically disclaim any implied warranties of merchantability</div><div>or fitness for a particular purpose. No warranty may be created or extended by sales representatives or</div><div>written sales materials. The advice and strategies contained herein may not be suitable for your situation.</div><div>You should consult with a professional where appropriate. Neither the publisher nor author shall be liable</div><div>for any loss of profit or any other commercial damages, including but not limited to special, incidental,</div><div>consequential, or other damages.</div>', 'Teknik Sipil', '-', 2, '2012-05-14', NULL, 9),
(71, 'Project Planning and Control', 'Eur Ing Albert Lester, CEng, FICE, FIMechE, FIStructE, FAPM AMSTERDAM', 'Butterworth-Heinemann', 2003, '-', 'Teknik Sipil', '-', 2, '2012-05-14', NULL, 2),
(72, 'PERHITUNGAN DISTRIBUSI TEGANGAN SISA DALAM PENGELASAN SAMBUNGAN–T PADA SISTEM PEMIPAAN', 'B. Bandriyana', '-', 2006, '<div>Studi tentang pemodelan dan perhitungan tegangan sisa akibat pengelasan</div><div>dilakukan pada sambungan-T dalam suatu sistem pemipaan. Untuk model</div><div>perhitungan diambil material standard las dengan data koefisien konduksi panas, k =</div><div>13,5x106 W/ °K.m2, dan angka muai termal, α = 13x10-6 mm/mm. Tegangan sisa</div><div>dihitung berdasarkan iterasi regangan yang timbul akibat distribusi suhu selama</div><div>pendinginan dari suhu pengelasan menuju temperatur ruang dengan simulasi suhu</div><div>pada daerah las sebesar 1200 °C. Perhitungan berdasarkan metode elemen hingga</div><div>dengan program ANSYS 5.4. model 3 dimensi. Distribusi tegangan menunjukkan</div><div>harga tegangan total pada bahan las berkisar antara 300 sampai dengan 400 MPa,</div><div>sedangkan pada material pipa antara 30 sampai 200 MPa. Konsentrasi tegangan</div><div>dapat diamati pada sambungan pengelasan yang terjadi pada bagian dalam dan</div><div>permukaan sambungan dengan tegangan maksimum sekitar 440 MPa. Dari</div><div>perhitungan ini dapat disimpulkan bahwa komputasi perhitungan sisa dapat</div><div>memprediksi harga rata-rata maupun harga maksimum tegangan sisa yang terjadi</div><div>akibat konsentrasi tegangan.</div>', 'Teknik mesin', 'tegangan sisa, pengelasan, sambungan-T, elemen hingga', 2, '2012-05-14', NULL, 1),
(73, 'Air-Conditioning and Refrigeration', 'Shan K. Wang, Zalman Lavan', 'CRC Press LLC', 1999, '-', 'Teknik mesin', '-', 2, '2012-05-14', NULL, 1),
(74, 'Minimization of welding residual stress and distortion in large structures', 'P. Michaleris - J Dantzig and D. Tortorelli', '-', 2000, '<div>Welding distortion in large structures is usually caused by buckling due to the residual stress. In</div><div>cases where the design is fixed and minimum weld size requirements are in place, the thermal</div><div>tensioning process has proven effective to reduce the welding residual stress below the critical</div><div>level and eliminate buckling distortion. In this work, a systematic design approach using</div><div>conventional finite element analysis, analytic sensitivity analysis, and nonlinear programming is</div><div>implemented to investigate and optimize the thermal tensioning process.</div>', 'Teknik Mesin', '-', 2, '2012-05-14', NULL, 1),
(75, 'Mekanika Fluida', 'Ferianto Raharjo', '-', 2000, '-', 'Teknik Mesin', '-', 2, '2012-05-14', NULL, 1),
(76, 'Tutorial Inventor 2009: Analisa Tegangan (Stress Analysis)', 'Agus Fikri Rosjadi', 'agus-fikri.blogspot.com', 2010, '-', 'Teknik Mesin', '-', 2, '2012-05-14', NULL, 3),
(77, 'Handbook of Machine Olfaction', 'T. C. Pearce, S. S. Schiffman, H.T. Nagle, J.W. Gardner', 'WILEY-VCH', 2000, '-', 'Teknik Mesin', '-', 2, '2012-05-14', NULL, 1),
(78, 'ELECTRIC POWER SYSTEMS A CONCEPTUAL INTRODUCTION', 'Alexandra von Meier', 'A JOHN WILEY & SONS, INC., PUBLICATION', 2006, '-', 'Teknik Mesin', '-', 2, '2012-05-14', NULL, 1),
(79, 'Lessons In Electric Circuits, Volume III { Semiconductors', 'Tony R. Kuphaldt', '-', 2006, '-', 'Teknik Elektro', '-', 2, '2012-05-14', NULL, 1),
(80, 'Design of Electrical Services for Buildings', 'Barrie Rigby', 'LONDON AND NEW YORK', 2005, '-', 'Teknik Elektro', '-', 2, '2012-05-14', NULL, 1),
(81, 'Circuit Analysis I with MATLAB® Computing and Simulink®/SimPowerSystems® Modeling', 'Steven T. Karris', 'Orchard Publications, Fremont, California', 2009, '-', 'Teknik Elektro', '-', 2, '2012-05-14', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_comment_post` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`id`, `content`, `status`, `create_time`, `author`, `email`, `url`, `post_id`) VALUES
(6, '<p>vcxhvh</p>', 2, 1327070572, 'hrryy', 'cbx@gjgj.com', '', 4),
(7, '<p>ada</p>', 2, 1327077603, 'saya', 'hendaar@gmail.com', '', 4),
(8, '<p>ada</p>', 1, 1327082224, 'hendar', 'hendaar@gmail.com', '', 5),
(9, '<p>ada ga</p>\r\n<p>&nbsp;</p>\r\n<p>ada</p>', 2, 1328735139, 'hendar', 'hendar@gmail.com', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment_thread`
--

CREATE TABLE IF NOT EXISTS `tbl_comment_thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) DEFAULT NULL,
  `isi` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `tanggalPost` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `thread_id` (`thread_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_comment_thread`
--

INSERT INTO `tbl_comment_thread` (`id`, `judul`, `isi`, `user_id`, `thread_id`, `tanggalPost`) VALUES
(1, 'komen ah', 'Keren Bro threadnya,..<br><br>Like This...,', 6, 2, '2012-04-27 22:41:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file_buku`
--

CREATE TABLE IF NOT EXISTS `tbl_file_buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_buku` int(11) NOT NULL,
  `files` varchar(137) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `tbl_file_buku`
--

INSERT INTO `tbl_file_buku` (`id`, `id_buku`, `files`) VALUES
(1, 12, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(2, 13, '937.pdf'),
(3, 14, '7.2 93-102 Nuryani. Efisiensi Pemupukan N tebu.pdf'),
(4, 14, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(5, 14, 'analisa-c-net.pdf'),
(6, 15, 'belajar-php-dengan-framework-code-igniter.pdf'),
(7, 15, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(8, 15, 'analisa-c-net.pdf'),
(9, 16, '7.2 93-102 Nuryani. Efisiensi Pemupukan N tebu.pdf'),
(10, 16, 'belajar-php-dengan-framework-code-igniter.pdf'),
(11, 16, 'ActualTraining.pdf'),
(12, 17, '7.2 93-102 Nuryani. Efisiensi Pemupukan N tebu.pdf'),
(13, 17, 'belajar-php-dengan-framework-code-igniter.pdf'),
(14, 17, 'ActualTraining.pdf'),
(15, 42, 'belajar-php-dengan-framework-code-igniter.pdf'),
(16, 42, 'analisa-c-net.pdf'),
(17, 43, 'ActualTraining.pdf'),
(18, 43, 'analisa-c-net.pdf'),
(19, 43, '60_Menit_Belajar_Sistem_Monitoring_Cacti.pdf'),
(20, 44, 'analisa-c-net.pdf'),
(21, 44, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(22, 45, 'analisa-c-net.pdf'),
(23, 45, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(24, 46, 'analisa-c-net.pdf'),
(25, 46, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(26, 47, 'analisa-c-net.pdf'),
(27, 47, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(28, 48, 'analisa-c-net.pdf'),
(29, 48, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(30, 49, 'belajar-php-dengan-framework-code-igniter.pdf'),
(31, 49, 'ActualTraining.pdf'),
(32, 50, 'belajar-php-dengan-framework-code-igniter.pdf'),
(33, 50, 'ActualTraining.pdf'),
(34, 50, '7.2 93-102 Nuryani. Efisiensi Pemupukan N tebu.pdf'),
(35, 50, 'BAB_III.pdf'),
(46, 51, '04 - AMIKOM_Yogyakarta_ANALISIS_ PERANCANGAN DAN IMPLEMENTASI SISTEM.pdf'),
(42, 51, '6.2.8.08.05.pdf'),
(38, 52, '6.2.8.08.05.pdf'),
(43, 51, 'analisa-c-net.pdf'),
(48, 53, '3_9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(49, 54, '53_3_9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(50, 55, 'Sertifikat_Brainometer_ASP.NET (Fundamental)_171011.pdf'),
(51, 55, 'Sertifikat_Brainometer_Web Dinamis (PHP & MySQL)_171011.pdf'),
(52, 56, '(Larman) applying uml and patterns.pdf'),
(64, 4, 'DMAdmin.pdf'),
(61, 57, 'Designer.pdf'),
(60, 57, 'DM_API_Ref.pdf'),
(66, 58, 'The_PHP_Anthology_Object_Oriented_PHP_Solutions_Volume_I_(SitePoint-2004)-0957921853.pdf'),
(65, 4, 'DM_API_Ref.pdf'),
(67, 59, '(Larman) applying uml and patterns.pdf'),
(68, 59, 'Amacom - Information Systems Project Management - How To Del.pdf'),
(69, 60, ''),
(70, 61, ''),
(71, 63, 'A Programmer’s Guide to ADO.NET in C#.pdf'),
(72, 63, 'Apress.Beginning.Object.Oriented.Programming.with.VB.2005.From.Novice.to.Professional.Nov.2005.pdf'),
(73, 64, '1847199585Agile.pdf'),
(74, 65, 'AGuidetoProjectManagement.pdf'),
(75, 66, 'ConstructionProjectManagementHandbook.pdf'),
(76, 67, 'EffectiveSoftwareProjectManagement.pdf'),
(77, 68, 'FundamentalsofProjectManagement.pdf'),
(78, 69, 'PracticalProjectManagement.pdf'),
(79, 70, 'ProjectManagement-ASystemsApproach10thEdition.pdf'),
(80, 71, 'ProjectPlanningandControl4thEdition.pdf'),
(81, 72, 'Perhitungan-Distribusi-TeganganSisa-Pada-Pengelasansam-SambunganT.pdf'),
(82, 73, 'ningandRefrigeration.MechanicalEngineeringHandbook.pdf'),
(83, 74, 'Minimizationofweldingresidualstressanddistortionin.pdf'),
(84, 75, 'MekanikaFluida.pdf'),
(85, 76, 'mastercambeginnertutorial.pdf'),
(86, 77, 'HandbookofMachineOlfactionElectronicNoseTechnology.pdf'),
(87, 78, 'electric_power_systems.pdf'),
(88, 79, 'TUTORIAL Elektronik.pdf'),
(89, 80, 'Design_of_Electrical_Services_for_Buildings_4th_edition_(2005).pdf'),
(90, 81, '1934404179Circuit Analysis I with MATLAB Computing and Simulink SimPowerSystems Modeling.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file_jurnal`
--

CREATE TABLE IF NOT EXISTS `tbl_file_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jurnal` int(11) NOT NULL,
  `files` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_file_jurnal`
--

INSERT INTO `tbl_file_jurnal` (`id`, `id_jurnal`, `files`) VALUES
(17, 4, 'Amacom - Information Systems Project Management - How To Del.pdf'),
(16, 4, '(Larman) applying uml and patterns.pdf'),
(15, 3, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(14, 3, 'analisa-c-net.pdf'),
(12, 2, 'belajar-php-dengan-framework-code-igniter.pdf'),
(11, 2, 'BAB_III.pdf'),
(13, 1, '7.2 93-102 Nuryani. Efisiensi Pemupukan N tebu.pdf'),
(18, 2, 'DMAdmin.pdf'),
(19, 5, 'Amacom - Information Systems Project Management - How To Del.pdf'),
(20, 6, 'Analisis pemrosesan paralel pada filter digital finite impulse response orde 3.pdf'),
(21, 7, 'Analisis pemrosesan paralel pada filter digital finite impulse response orde 3.pdf'),
(22, 8, 'Analisis pemrosesan paralel pada filter digital finite impulse response orde 3.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jurnal`
--

CREATE TABLE IF NOT EXISTS `tbl_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `penulis` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `abstrak` text COLLATE utf8_unicode_ci NOT NULL,
  `kategori` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `waktu_buat` date DEFAULT NULL,
  `waktu_edit` date DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_jurnal`
--

INSERT INTO `tbl_jurnal` (`id`, `judul`, `penulis`, `abstrak`, `kategori`, `status`, `waktu_buat`, `waktu_edit`, `download`) VALUES
(3, 'test', 'test', '<p>test</p>', 'Teknik Informatika', 2, NULL, NULL, NULL),
(2, 'membara', 'suhendar', '<p>panas</p>', 'umum', 2, '0000-00-00', '0000-00-00', NULL),
(4, 'basu', 'hendar', '<p>baru</p>', 'baru', 2, NULL, NULL, NULL),
(5, 'jurnal hendar', 'hendar', 'ini punya hendar', 'umum', 2, NULL, NULL, NULL),
(6, 'Analisis pemrosesan paralel pada filter digital finite impulse response orde 3', 'Karel Octavianus Bachri', '<font face="arial" size="2">Pemerosesan paralel adalah sebuah teknik \r\nyang digunakan untuk membuat sistem bekerja lebih cepat. Pemerosesan \r\nparalel dilakukan dengan membuat beberapa elemen pemroses yang bekerja \r\nbersama-sama. Tujuan makalah ini adalah menganalisis rancangan \r\npemerosesan paralel filter digital Finite Impulse Response (FIR) dan \r\nmembandingkan hasilnya dengan rancangan tilter digital FIR sekuensial. \r\nDengan teknik pemerosesan paralel, luas area yang ditempati filter \r\ndigital akan bertambah besar, tetapi waktu proses lebih cepat dan \r\nkonsumsi daya dapat dikurangi dengan mengurangi tegangan catu.</font>', 'Teknik Elektro', 2, NULL, NULL, NULL),
(7, 'Analisis pemrosesan paralel pada filter digital finite impulse response orde 3', 'Karel Octavianus Bachri', '<font face="arial" size="2">Pemerosesan paralel adalah sebuah teknik \r\nyang digunakan untuk membuat sistem bekerja lebih cepat. Pemerosesan \r\nparalel dilakukan dengan membuat beberapa elemen pemroses yang bekerja \r\nbersama-sama. Tujuan makalah ini adalah menganalisis rancangan \r\npemerosesan paralel filter digital Finite Impulse Response (FIR) dan \r\nmembandingkan hasilnya dengan rancangan tilter digital FIR sekuensial. \r\nDengan teknik pemerosesan paralel, luas area yang ditempati filter \r\ndigital akan bertambah besar, tetapi waktu proses lebih cepat dan \r\nkonsumsi daya dapat dikurangi dengan mengurangi tegangan catu.</font>', 'Teknik Elektro', 2, '2012-04-27', NULL, NULL),
(8, 'Analisis pemrosesan paralel pada filter digital finite impulse response orde 3', 'Karel Octavianus Bachri', '<font face="arial" size="2">Pemerosesan paralel adalah sebuah teknik \r\nyang digunakan untuk membuat sistem bekerja lebih cepat. Pemerosesan \r\nparalel dilakukan dengan membuat beberapa elemen pemroses yang bekerja \r\nbersama-sama. Tujuan makalah ini adalah menganalisis rancangan \r\npemerosesan paralel filter digital Finite Impulse Response (FIR) dan \r\nmembandingkan hasilnya dengan rancangan tilter digital FIR sekuensial. \r\nDengan teknik pemerosesan paralel, luas area yang ditempati filter \r\ndigital akan bertambah besar, tetapi waktu proses lebih cepat dan \r\nkonsumsi daya dapat dikurangi dengan mengurangi tegangan catu.</font>', 'Teknik Elektro', 2, '2012-04-27', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori_thread`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori_thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_kategori_thread`
--

INSERT INTO `tbl_kategori_thread` (`id`, `kategori`) VALUES
(5, 'Teknik Informatika'),
(6, 'Teknik Sipil'),
(7, 'Teknik Mesin'),
(8, 'Teknik Elektro'),
(9, 'Umum');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kp`
--

CREATE TABLE IF NOT EXISTS `tbl_kp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `npm` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `pembimbing` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pembimbing_lapangan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi_kp` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_kp`
--

INSERT INTO `tbl_kp` (`id`, `nama`, `npm`, `tahun`, `pembimbing`, `pembimbing_lapangan`, `judul`, `deskripsi`, `jurusan`, `lokasi_kp`, `lokasi`) VALUES
(1, 'Agus Priyanto', '05215410381', 2009, 'Kodarsyah, S.Kom.', 'Dr. Yogi Sirodz Gaos, Ir., M.T.', 'Desain LAN Berbasis Wireless di PT. Intan Prima Kalorindo', '<p>sejak masyarakatnya internet dan dipasarkannya sistem operasi windows 95 oleh microsoft.</p>', 'Teknik Informatika', 'PT. Intan Prima Kalorindo', 'LKP 57 TI'),
(2, 'Adi Permana Ramdan', '06215410354', 2009, 'Novita Br.Ginting, S.Kom', 'Budiawan', 'Aplikasi Sistem Absensi Karyawan dengan Metoda Barcode', '<span style="font-style: italic;">Time Client</span> merupakan aplikasi yang digunakan untuk mengsinkronnisasikan waktu di komputer lokal (<span style="font-style: italic;">client</span>) dengan komputer <span style="font-style: italic;">server</span>. Secara otomatis, sesuai interval waktu yang telah ditentukan, <span style="font-style: italic;">Time client</span> akan menyamakan waktu komputer lokal dengan komputer <span style="font-style: italic;">server</span>.<br>', 'Teknik Informatika', 'PT. GOODYEAR INDONESIA Tbk. BOGOR', 'LKP 206 TI'),
(3, 'Adi Permana Ramdan', '06215410354', 2009, 'Novita Br.Ginting, S.Kom', 'Budiawan', 'Aplikasi Sistem Absensi Karyawan dengan Metoda Barcode', 'Time Client merupakan aplikasi yang digunakan untuk mengsinkronnisasikan waktu di komputer lokal client dengan komputer server. Secara otomatis, sesuai interval waktu yang telah ditentukan,Time clientakan menyamakan waktu komputer lokal dengan komputer serve.<br><br>', 'Teknik Informatika', 'PT. GOODYEAR INDONESIA Tbk. BOGOR', 'LKP 206 TI');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_level_admin` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_level_admin`
--

INSERT INTO `tbl_level_admin` (`id_level`, `level`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lookup`
--

CREATE TABLE IF NOT EXISTS `tbl_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_lookup`
--

INSERT INTO `tbl_lookup` (`id`, `name`, `code`, `type`, `position`) VALUES
(1, 'Draft', 1, 'PostStatus', 1),
(2, 'Published', 2, 'PostStatus', 2),
(3, 'Archived', 3, 'PostStatus', 3),
(4, 'Pending Approval', 1, 'CommentStatus', 1),
(5, 'Approved', 2, 'CommentStatus', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` text COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_author` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `title`, `content`, `tags`, `status`, `create_time`, `update_time`, `author_id`) VALUES
(4, 'test', '<p>bla ughkugug gc,htd,hc</p>', 'test', 2, 1327070480, 1327070531, 4),
(5, 'baru', '<p>test</p>', 'baru', 2, 1327082178, 1327082178, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reputation`
--

CREATE TABLE IF NOT EXISTS `tbl_reputation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `jenis` tinyint(1) NOT NULL,
  `pemberi_id` int(11) NOT NULL,
  `penerima_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pemberi_id` (`pemberi_id`),
  KEY `penerima_id` (`penerima_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_reputation`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_ta`
--

CREATE TABLE IF NOT EXISTS `tbl_ta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `npm` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `pembimbing1` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pembimbing2` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `abstrak` text COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_ta`
--

INSERT INTO `tbl_ta` (`id`, `nama`, `npm`, `tahun`, `pembimbing1`, `pembimbing2`, `judul`, `abstrak`, `jurusan`, `lokasi`) VALUES
(1, 'Mahmud Suhendar', '07215410445', 2011, 'Foni agus ', 'Peti', 'RANCANG BANGUN PERPUSTAKAAN DIGITAL BERBASIS WEB MENGGUNAKAN DESIGN PATTERN MVC DI FAKULTAS TEKNIK UNIVERSITAS IBN KHALDUN BOGOR', 'ga jelas', 'Teknik Informatika', '09871'),
(2, 'Ikhsan Arhadi', '05215410400', 2010, 'Muhamad Lutfi, S.T., M.Kom', 'Dewi Primasari, S.Si., M.M', 'PERANCANGAN SISTEM APLIKASI PEMILIHAN CALON PEJABAT STRUKTURAL DENGAN MENGGUNAKAN METODE ANALITYCAL HIERARCHY PROCESS', 'Berakhirnya periode suatu masa jabatan bagi sejumlah pejabat struktural di lingkungan Akademik sudah pasti dilakukan suatu kegiatan yaitu proses pemilihan calon pejabat struktural di lingkungan Akademik dimana bakal calon tersebut diambil dari lingkungan Akademik itu sendiri. Analisis sistem kebutuhan meliputi:<br><ol><li>Analisis sistem pemilihan calon pejabat struktural yang sedang berjalan,</li><li>Sistem yang diusulkan.</li></ol>Perancangan sistem aplikasi pemilihan calon pejabat struktural dengan menggunakan metode <span style="font-style: italic;">AHP</span> meliputi:<br><ul><li>Analisa <span style="font-style: italic;">performance</span></li><li>Analisa<span style="font-style: italic;"> information</span></li><li>Analisa<span style="font-style: italic;"> economy</span></li><li>Analisa <span style="font-style: italic;">control</span></li><li>Analisa<span style="font-style: italic;"><span style="font-style: italic;"> </span>efficiency</span></li><li>Analisa<span style="font-style: italic;"> service</span></li></ul><p>Maka rancangan yang diusulkan akan memiliki 6 komponen utama, yaitu: proses analisis metode <span style="font-style: italic;">AHP</span> berupa diagram alir utama, diagram alir sistem informasi pemilihan calon pejabat struktural, diagram alir <span style="font-style: italic;">AHP</span> kriteria , diagram alir <span style="font-style: italic;">AHP</span> pegawai, diagram alir hasil analisis. Data model perancangan sistem aplikasi pemilihan calon pejabat struktural meliputi:</p><ol><li>logical data model</li><li>physical data model</li></ol><span style=" font-style: italic;"></span>', 'Teknik Informatika', 'LTA 789 TI'),
(3, 'Heri Hermawan', '07215410107', 2012, 'Budi Susetyo, Ir., M.Sc.IT.', 'Safarudin A Hidayat, S.Kom.', 'RANCANG BANGUN APLIKASI BERBASIS WEB PEMILIHAN CALON KETUA BEM MENGGUNAKAN METODE RAPID APPLICATION DEVELOPMENT  DI UNIVERSITAS ', '<!--[if gte mso 9]><xml>\r\n <o:OfficeDocumentSettings>\r\n  <o:RelyOnVML/>\r\n  <o:AllowPNG/>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:DontVertAlignCellWithSp/>\r\n   <w:DontBreakConstrainedForcedTables/>\r\n   <w:DontVertAlignInTxbx/>\r\n   <w:Word11KerningPairs/>\r\n   <w:CachedColBalance/>\r\n  </w:Compatibility>\r\n  <m:mathPr>\r\n   <m:mathFont m:val="Cambria Math"/>\r\n   <m:brkBin m:val="before"/>\r\n   <m:brkBinSub m:val="&#45;-"/>\r\n   <m:smallFrac m:val="off"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val="0"/>\r\n   <m:rMargin m:val="0"/>\r\n   <m:defJc m:val="centerGroup"/>\r\n   <m:wrapIndent m:val="1440"/>\r\n   <m:intLim m:val="subSup"/>\r\n   <m:naryLim m:val="undOvr"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"\r\n  DefSemiHidden="true" DefQFormat="false" DefPriority="99"\r\n  LatentStyleCount="267">\r\n  <w:LsdException Locked="false" Priority="0" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>\r\n  <w:LsdException Locked="false" Priority="9" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>\r\n  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>\r\n  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>\r\n  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>\r\n  <w:LsdException Locked="false" Priority="10" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Title"/>\r\n  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>\r\n  <w:LsdException Locked="false" Priority="11" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>\r\n  <w:LsdException Locked="false" Priority="22" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>\r\n  <w:LsdException Locked="false" Priority="20" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="59" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Table Grid"/>\r\n  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>\r\n  <w:LsdException Locked="false" Priority="1" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>\r\n  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>\r\n  <w:LsdException Locked="false" Priority="34" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>\r\n  <w:LsdException Locked="false" Priority="29" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>\r\n  <w:LsdException Locked="false" Priority="30" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>\r\n  <w:LsdException Locked="false" Priority="60" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="61" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="62" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="63" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="64" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="65" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="66" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="67" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="68" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="69" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="70" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Dark List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="71" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="72" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="73" SemiHidden="false"\r\n   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>\r\n  <w:LsdException Locked="false" Priority="19" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="21" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>\r\n  <w:LsdException Locked="false" Priority="31" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>\r\n  <w:LsdException Locked="false" Priority="32" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>\r\n  <w:LsdException Locked="false" Priority="33" SemiHidden="false"\r\n   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>\r\n  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>\r\n  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:"Table Normal";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-qformat:yes;\r\n	mso-style-parent:"";\r\n	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\n	mso-para-margin-top:0cm;\r\n	mso-para-margin-right:0cm;\r\n	mso-para-margin-bottom:10.0pt;\r\n	mso-para-margin-left:0cm;\r\n	line-height:115%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:"Calibri","sans-serif";\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-fareast-font-family:"Times New Roman";\r\n	mso-fareast-theme-font:minor-fareast;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:"Times New Roman";\r\n	mso-bidi-theme-font:minor-bidi;}\r\n</style>\r\n<![endif]-->\r\n\r\n<p class="MsoNormal"><b style="mso-bidi-font-weight:normal">RANCANG BANGUN\r\nAPLIKASI BERBASIS <i style="mso-bidi-font-style:normal">WEB</i> PEMILIHAN CALON\r\nKETUA (BEM) MENGGUNAKAN METODE <i style="mso-bidi-font-style:normal">RAPID\r\nAPPLICATION DEVELOPMENT</i> DI UNIVERSITAS IBN KHALDUN BOGOR</b>, pemilihan\r\nketua BEM dilakukan manakala periode masa jabatan telah berakhir, sehingga\r\nperlu dilakukan suatu kegiatan yaitu proses pemilihan kembali untuk menduduki\r\njabatan ketua periode berikutnya. Pelaksanaan pemilihan pada umumnya secara\r\nkonvesional, sehingga memiliki resiko terjadinya human error dalam proses pengolahan\r\ndata, memungkinkan ekplorasi informasi yang minim dan kurang jelasnya kriteria\r\nyang harus dimilki oleh para calon ketua BEM. Untuk mengatasi berbagai kendala\r\ntersebut diperlukan sistem penunjang keputusan yang diharapkan membantu didalam\r\npemilihan ketua BEM. Analisis kebutuhan meliputi (1) Analisis kebutuhan\r\nfungsional, (2) analisis kebutuhan pengguna, (3) analisis masukan sistem, (4)\r\nanalisis keluaran sistem, (5) analisis proses bisnis sistem lama, dan (6)\r\nanalisis proses bisnis sistem baru. Perancangan dengan UML aplikasi pemilihan\r\ncalon ketua BEM meliputi: a) diagram konteks, b) pelaku sistem, c) <i style="mso-bidi-font-style:normal">list use case</i>, d) diagram <i style="mso-bidi-font-style:normal">use case</i>, e) diagram use case\r\nketergantungan, f) <i style="mso-bidi-font-style:normal">activity diagram</i>,\r\ng) <i style="mso-bidi-font-style:normal">sequence diagram</i>, h) <i style="mso-bidi-font-style:normal">collaboration diagram</i>, i) <i style="mso-bidi-font-style:normal">component diagram</i>, j) <i style="mso-bidi-font-style:normal">deployment diagram</i>, k) <i style="mso-bidi-font-style:normal">class diagram</i>, l) <i style="mso-bidi-font-style:\r\nnormal">use case narrative</i> persyaratan bisnis, m) <i style="mso-bidi-font-style:\r\nnormal">Use case narrative</i> analisis sistem, n) <i style="mso-bidi-font-style:\r\nnormal">use case narrative</i> disain sistem, o) konsepsualisasi, p)\r\npembentukan sistem pendukung keputusan dengan metode AHP, q) perancangan <i style="mso-bidi-font-style:normal">database</i>.</p>\r\n\r\n', 'Teknik Informatika', 'TA00712'),
(4, 'Afri Herianto', '05215410405', 2009, 'Muhamad Lufti S.T.,M.Kom', 'Oke Hendradhy, S.Kom', 'REKAYASA APLIKASI SISTEM PERGUDANGAN', '<span style="font-style: normal; "><span class="Apple-tab-span" style="white-space:pre">	</span>PT Intan Prima Kalorindo bergerak dalam bidang manufaktur </span><span style="font-style: italic;">Exchangger </span>untuk berbagai keperluan. Ruang lingkup kegiatan bisnis &nbsp;yang dikelola oleh PT Intan Prima Kalorindo mencakup disain dan manufaktur <span style="font-style: italic;">Heat Exchangger. </span>Salah satu problematika<span style="font-style: italic;">&nbsp;</span>yang terjadi pada perusahaan tersebut adalah bagaiman mengelola stok gudang, sehingga perlu dilakukan suatu pendekatan &nbsp;sistem yang berfungsi untuk membantu dalam pengelolaan stok &nbsp;gudang. Dengan model rekayasa perangkat lunak aplikasi kebutuhan akan penataan sistem pergudangan dapat membantu PT Intan Prima Kalorindo dalam hal &nbsp;pengelolaan sistem pergudangna. Studi atas rekayasa perangkat lunak sistem pergudangan ini merupakan salah satu bentuk implementasi tugas akhir dalam penataan sistem aplikasi pergudangan di lingkungan PT Intan Prima Kalorindo.', 'Teknik Informatika', 'SKRIPSI 17 TI');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag`
--

CREATE TABLE IF NOT EXISTS `tbl_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `tbl_tag`
--

INSERT INTO `tbl_tag` (`id`, `name`, `frequency`) VALUES
(1, 'h', 1),
(2, 'f', 2),
(3, 'n', 1),
(4, 'z', 2),
(6, 'v', 1),
(7, 'c', 1),
(8, 'd', 3),
(10, 'Teknik Informatika', 8),
(11, 'Umum', 4),
(12, 'test', 1),
(13, 'Sertifikasi', 1),
(14, 'baru', 1),
(15, 'Teknik Mesin', 8),
(16, 'Web', 1),
(17, 'Teknik Sipil', 7),
(18, 'Teknik Elektro', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thread`
--

CREATE TABLE IF NOT EXISTS `tbl_thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `tanggalPost` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `kategori_id` (`kategori_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_thread`
--

INSERT INTO `tbl_thread` (`id`, `judul`, `isi`, `user_id`, `kategori_id`, `tanggalPost`) VALUES
(1, 'test', 'test', 5, 5, '2012-04-24 18:32:56'),
(2, 'Lagi ngetest thread nih', 'tolong dikomen yah!!<br>', 5, 5, '2012-04-27 22:33:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_threadstart`
--

CREATE TABLE IF NOT EXISTS `tbl_threadstart` (
  `is` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  PRIMARY KEY (`is`),
  KEY `user_id` (`user_id`),
  KEY `thread_id` (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_threadstart`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `Password` char(40) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `LastLogged` datetime DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `RegisterDate` datetime DEFAULT NULL,
  `IsConfirmed` tinyint(1) DEFAULT '0',
  `ConfirmSeed` char(5) DEFAULT 'abcde',
  `UserGroupId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `i_t_user_unique_email` (`Email`),
  KEY `FK_t_user` (`UserGroupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`Id`, `UserName`, `Password`, `Email`, `LastLogged`, `IsActive`, `RegisterDate`, `IsConfirmed`, `ConfirmSeed`, `UserGroupId`) VALUES
(5, 'hendar', '829b36babd21be519fa5f9353daf5dbdb796993e', 'hendar@127.0.0.1', '2012-10-01 14:06:17', 1, '2012-04-15 12:15:30', 1, 'c35a2', 1),
(6, 'mahmud', '334bf206aca8b1265a47b84db6ef96c3657195ee', 'mahmud@127.0.0.1', '2012-04-27 22:40:01', 1, '2012-04-17 00:52:59', 1, '450c8', 2),
(7, 'kenzi', '406498aecfa2c0e9252c09391ecbf5acb7907862', 'kenzi@127.0.0.1', NULL, 0, '2012-04-27 11:21:16', 0, '5313f', 2),
(8, 'momo', '77add44f8f13cf5b3298a7833613aca42430386d', 'momo@127.0.0.1', NULL, 1, '2012-04-27 16:54:39', 1, 'e86c2', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enkrip` varchar(50) NOT NULL,
  `email` varchar(128) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_user_admin`
--

INSERT INTO `tbl_user_admin` (`id`, `username`, `password`, `enkrip`, `email`, `id_level`) VALUES
(4, 'hendar', '17793c7e782bc4270849ef8d45da2be3', '4f194568791af9.77207572', 'hendaar@gmail.com', 2),
(5, 'admin', '73037d8dd75d2dc95697e29a772a85de', '4ef2e5528616f6.41101946', 'admin@yahoo.com', 1),
(6, 'masuk', 'ca55e715f39436733560dc40a7648172', '4f181a1388d2e2.84134036', 'hendar123@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_level`
--

CREATE TABLE IF NOT EXISTS `tbl_user_level` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `i_t_user_level_unique_name` (`Name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_user_level`
--

INSERT INTO `tbl_user_level` (`Id`, `Name`) VALUES
(1, 'Administrator'),
(2, 'User');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD CONSTRAINT `FK_comment_post` FOREIGN KEY (`post_id`) REFERENCES `tbl_post` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_comment_thread`
--
ALTER TABLE `tbl_comment_thread`
  ADD CONSTRAINT `tbl_comment_thread_ibfk_1` FOREIGN KEY (`thread_id`) REFERENCES `tbl_thread` (`id`);

--
-- Constraints for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `tbl_post_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user_admin` (`id`);

--
-- Constraints for table `tbl_thread`
--
ALTER TABLE `tbl_thread`
  ADD CONSTRAINT `tbl_thread_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `tbl_kategori_thread` (`id`);

--
-- Constraints for table `tbl_user_admin`
--
ALTER TABLE `tbl_user_admin`
  ADD CONSTRAINT `tbl_user_admin_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `tbl_level_admin` (`id_level`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
