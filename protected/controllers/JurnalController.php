<?php

class JurnalController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	private $_model;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('search','index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('download'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete'),
				'expression'=>'Yii::app()->user->id_level==1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$jurnal=$this->loadModel();
		
		$criteria=new CDbCriteria(array(
			'condition'=>'id_jurnal='.$_GET['id'],
		));

		$dataProvider=new CActiveDataProvider('FileJurnal', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['postsPerPage'],
			),
			'criteria'=>$criteria,
		));
		$dataProvider2=Jurnal::model()->similiar($tag,$id);
		
		$this->render('view',array(
		'model'=>$jurnal,
		'model2'=>$dataProvider,
		'dataProvider2'=>$dataProvider2,
		));
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Jurnal;
		$model2 = new FileJurnal();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jurnal']) && isset($_FILES['FileJurnal']))
		{
			$model->attributes=$_POST['Jurnal'];
			$model->waktu_buat=date('Y-m-d');
			if($model->save())
			{
				for($i=0;$i<count($_FILES['FileJurnal']["name"]['files']);$i++)
				{
					$model2 = new FileJurnal();
					
					$model2['id_jurnal'] = $model['id'];
					$model2['files'] = $_FILES['FileJurnal']["name"]['files'][$i];
					$model2->save();
				}
				
				$data = $_FILES['FileJurnal'];
				
				for($i=0;$i<count($data["tmp_name"]["files"]);$i++)
				{
					$tmp_name = $data["tmp_name"]["files"][$i];
					$name = $data["name"]["files"][$i];
					move_uploaded_file($tmp_name, "./file/jurnal/".$model['id']."_$name");
				}
								
				$this->redirect(array('view','id'=>$model->id));
			}
				
		}

		$this->render('create',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$data=new FileJurnal('search');
		$data->unsetAttributes();  // clear any default values
		$_GET['FileJurnal']['id_jurnal'] = $id;
		if(isset($_GET['FileJurnal']))
			$data->attributes=$_GET['FileJurnal'];

			
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jurnal']))
		{
			$model->attributes=$_POST['Jurnal'];
			if($model->save())
		{
				for($i=0;$i<count($_FILES['FileJurnal']["name"]['files']);$i++)
				{
					$model2 = new FileJurnal();
					
					$model2['id_jurnal'] = $model['id'];
					$model2['files'] = $_FILES['FileJurnal']["name"]['files'][$i];
					$model2->save();
				}
				
				$data = $_FILES['FileJurnal'];
				
				for($i=0;$i<count($data["tmp_name"]["files"]);$i++)
				{
					$tmp_name = $data["tmp_name"]["files"][$i];
					$name = $data["name"]["files"][$i];
					move_uploaded_file($tmp_name, "./file/jurnal/".$model['id']."_$name");
				}
								
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'model2'=>FileJurnal::model()->findAll("id_jurnal", $id),
			'data'=>$data,
			'newFileJurnal'=>new FileJurnal(),
		));
	}
	
	public function actionDownload()
	{
		$uncloak = new EFileCloaker();
		$uncloak->download();
		
		$this->redirect(array('/site/index'));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			$modelFileJurnal = FileJurnal::model()->findAll("id_jurnal=".$model->id);
			foreach($modelFileJurnal as $data)
			{
				if(!unlink($_SERVER['DOCUMENT_ROOT'] ."/yii/digilib" . "/file/jurnal/" . $data->id_jurnal . "_" . $data->files))
				{
					throw new CException('File cant be deleted.');
				}
			}
			$model->delete();
			
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$criteria=new CDbCriteria(array(
			'condition'=>'status='.Jurnal::STATUS_PUBLISHED,
			'order'=>'waktu_buat DESC',
		));
		
		$dataProvider=new CActiveDataProvider('Jurnal', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['postsPerPage'],
			),
			'criteria'=>$criteria,
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Jurnal('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Jurnal']))
			$model->attributes=$_GET['Jurnal'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionSearch()
	{
		$model=new Jurnal('search');
		if(isset($_GET['Jurnal']))
			$model->attributes=$_GET['Jurnal'];
		$this->render('search',array(
			'model'=>$model,
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			{
				if(Yii::app()->user->isGuest)
					$condition='status='.Jurnal::STATUS_PUBLISHED.' OR status='.Jurnal::STATUS_ARCHIVED;
				else
					$condition='';
				$this->_model=Jurnal::model()->findByPk($_GET['id'], $condition);
			}
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jurnal-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
?>
