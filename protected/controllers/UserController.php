<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','register', 'confirm', 'resetpass', 'changepass'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
                'expression'=>'Yii::app()->user->id_level<=1',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$dataProvider2=new CActiveDataProvider('Thread');
		$model2=new Thread('search');
		$_GET['Thread']['user_id'] =$id;
		if(isset($_GET['Thread']))
			$model2->attributes=$_GET['Thread'];
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'model2'=>$model2,
			'dataProvider2'=>$dataProvider2,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->Id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
    
    public function actionResetPass()
    {
      $model = new User;
      
      if(isset($_POST['User']))
      {
        $model->attributes = $_POST['User'];
        
        $model = User::model()->find("Email = '".$model->Email."'");
        if($model === null)
        {
          $model = new User;
          Yii::app()->user->setFlash('info-error', "Email tidak valid.");
        } else
        {
          $newPass = substr(md5(mt_rand(1000000, 9999999)), 5,8);
          $model->Password = $newPass;
          $model->IsNewPassword = true;
          
          if($model->validate(array('Email')))
          {
            if($model->update())
            {
              if(Mail::sendmail($model->Email, $model->UserName, "Sandi baru", "Sandi baru adalah " . $newPass . "."))
              {
                Yii::app()->user->setFlash('info-message', "Sandi baru telah dikirim melalui email " . $model->Email);
              } else {
                Yii::app()->user->setFlash('info-error', "Sandi telah diubah, namun pengiriman email gagal. Silakan kontak administrator situs ini.");
              }
            } else {
              Yii::app()->user->setFlash('info-error', "Gagal mengirimkan sandi baru. Silakan kontak administrator situs ini.");
            }
          }
        }
      }
      
//      throw new CHttpException(404,'Method not implemented.');
      $this->render('_reset_pass', array('model'=>$model));
    }
    
    public function actionChangePass()
    {
      $model=$this->loadModel(Yii::app()->user->Id);
      $model->setScenario('changepass');
      
      if(isset($_POST['User']))
      {
        $model->attributes = $_POST['User'];
          
        if($model->validate())
        {
          $model->IsNewPassword = true;
          $model->Password = $model->NewPassword;
          if($model->update())
          {
            Yii::app()->user->setFlash('info-message', "Sandi telah diubah.");
          } else {
            Yii::app()->user->setFlash('info-error', "Sandi gagal diubah. Mohon hubungi adminsitrator situs ini.");
          }
        }
          
      }
      
      $this->render('_change_pass', array('model'=>$model));
      //OldPassword, NewPassword, ConfirmPassword
    }
    
    public function actionRegister()
	{
        $model=new User;
        $model->setScenario('register');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
            
			if($model->save())
            {
				$confirm_link = Yii::app()->createAbsoluteUrl('user/confirm', array('uid'=>$model->Id, 'code'=>$model->getConfirmationCode()));
                $body_msg = "Pendaftaran berhasil dilakukan.<br />Untuk melakukan verifikasi email, tekan tautan berikut.<br /><br />" .
                            CHtml::link($confirm_link, $confirm_link)
                             ;
                
                Mail::sendmail($model->Email, $model->UserName, "Konfirmasi Email", $body_msg);
                Yii::app()->user->setFlash('info-message', "Pendaftaran berhasil dilakukan.<br />Silakan cek email untuk melaukan aktivasi.");
                $this->redirect(array('site/index'));
            }
		}

		$this->render('register',array(
			'model'=>$model,
		));
	}
    
    public function actionConfirm($uid, $code)
    {
      if($uid == "" || $code == "")
      {
		Yii::trace("Uid or confirmation code is empty or invalid.");
        throw new CHttpException(400,'Invalid operation. Do not repeat what you did.');
      } else
      { 
        $model = User::model()->findByPk($uid);
        
        // Kick out brute force attempts
        if($model === null)
        {
          Yii::trace("User not found.");
          throw new CHttpException(400,'Invalid operation. Do not repeat what you did.');
        }
        
        // Kick out outdated registered users
//        $interval = date_diff(DateTime::createFromFormat(Yii::app()->params['dbDateFormat'], $model->RegisterDate), date_create("now"));
//        if($interval->format('%R')=="+" && $interval->format('%a') > 1)
//        {
//          Yii::app()->user->setFlash('info-error', "Konfirmasi email gagal dikakukan.<br />" .
//                  "Silakan hubungi administrator sistem kami untuk bantuan lebih lanjut.");
//          $this->redirect(array("site/index"), true);
//        }
        
        // Kick out users that have confirmed
        if($model->IsConfirmed == 1)
        {
          Yii::app()->user->setFlash('info-message', "Konfirmasi email telah dikakukan.<br />" .
                    "Anda sudah bisa menggunakan akun anda untuk masuk ke dalam sistem.");
          
          $this->redirect(array("site/login", true));
        }
        
        // Check the confirmation code
        if($model->getConfirmationCode() == $code)
        {
          $model->IsActive = 1;
          $model->IsConfirmed = 1;
          if($model->update())
          {
            Yii::app()->user->setFlash('info-message', "Konfirmasi email berhasil dikakukan.<br />" .
                    "Sekarang anda bisa menggunakan akun anda untuk masuk ke dalam sistem.");
            
            $this->redirect(array("site/login", true));
          }
        }
        
        Yii::app()->user->setFlash('info-error', "Konfirmasi email gagal dikakukan.<br />" .
                  "Silakan hubungi administrator sistem kami untuk bantuan lebih lanjut.");
        $this->redirect(array("site/index"));
      }
    }
    
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
            $model->attributes=$_POST['User'];
            
            if($model->Password != "")
            {
              $model->IsNewPassword = true;
            }
            
			if($model->save(true, HGeneral::removeEmptyAttribute($model, $_POST['User'])))
				$this->redirect(array('view','id'=>$model->Id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
