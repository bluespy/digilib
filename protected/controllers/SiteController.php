<?php

class SiteController extends Controller
{
	public $layout='column1';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionLoadWordList()
    {    
      $word = Yii::app()->request->getParam("wordid");
      $filter = "judul LIKE '%$word%'";
      
      $dataProviderWords=new CActiveDataProvider(Buku::model()->word_only());
      $dataProviderWords->criteria->condition = $filter;
      $dataProviderWords->getData(true);
      echo '</br>';
      echo 'BUKU';
      echo $this->renderPartial('/site/_wordListContainer', array('dataProviderWords'=>$dataProviderWords));

      $dataProviderWords=new CActiveDataProvider(Jurnal::model()->word_only());
      $dataProviderWords->criteria->condition = $filter;
      $dataProviderWords->getData(true);
      echo '</br>';
      echo 'JURNAL';
      echo $this->renderPartial('/site/_wordListContainer', array('dataProviderWords'=>$dataProviderWords));
      
      $dataProviderWords=new CActiveDataProvider(Ta::model()->word_only());
      $dataProviderWords->criteria->condition = $filter;
      $dataProviderWords->getData(true);
      echo '</br>';
      echo 'SKRIPSI';
      echo $this->renderPartial('/site/_wordListContainer', array('dataProviderWords'=>$dataProviderWords));
     
      $dataProviderWords=new CActiveDataProvider(Kp::model()->word_only());
      $dataProviderWords->criteria->condition = $filter;
      $dataProviderWords->getData(true);
      echo '</br>';
      echo 'LAPORAN KERJA PRAKTEK';
      echo $this->renderPartial('/site/_wordListContainer', array('dataProviderWords'=>$dataProviderWords));
     
      Yii::app()->end();
    }
    
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KategoriThread');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	public function actionKoleksi()
	{
		$dataProvider=new CActiveDataProvider('KategoriThread');
		$this->render('koleksi',array(
			'dataProvider'=>$dataProvider,
		));
	}
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}