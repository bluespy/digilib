<?php

class BukuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','search'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('download'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete'),
				'expression'=>"Yii::app()->user->id_level==1",
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$buku=$this->loadModel($id);
		$old=$buku->view;
		$buku->view= $old+1;
		$buku->save();
		$tag=$buku->tags;
		$id=$buku->id;
		
		$criteria=new CDbCriteria(array(
			'condition'=>'id_buku='.$_GET['id'],
		));

		$dataProvider=new CActiveDataProvider('FileBuku', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['postsPerPage'],
			),
			'criteria'=>$criteria,
		));
		$dataProvider2=Buku::model()->similiar($tag,$id);
		
		$this->render('view',array(
		'model'=>$buku,
		'model2'=>$dataProvider,
		'dataProvider2'=>$dataProvider2,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Buku;
		$model2 = new FileBuku();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buku']) && isset($_FILES['FileBuku']))
		{
			$model->attributes=$_POST['Buku'];
			$model->tanggal=date('Y-m-d');
		
			
			if($model->save())
			{
				for($i=0;$i<count($_FILES['FileBuku']["name"]['files']);$i++)
				{
					if(strlen($_FILES['FileBuku']['name']['files'][$i])>0)
					{
						$model2 = new FileBuku();
						
						$model2['id_buku'] = $model['id'];
						$model2['files'] = $_FILES['FileBuku']["name"]['files'][$i];
						$model2->save();
					}
				}
				
				$data = $_FILES['FileBuku'];
				
				for($i=0;$i<count($data["tmp_name"]["files"]);$i++)
				{
					$tmp_name = $data["tmp_name"]["files"][$i];
					$name = $data["name"]["files"][$i];
					move_uploaded_file($tmp_name, "./file/buku/".$model['id']."_$name");
				}
								
				$this->redirect(array('view','id'=>$model->id));
			}
		}

			$this->render('create',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}

	public function actionDownload($id)
	{		
		$uncloak = new EFileCloaker();
		$uncloak->download();
//		$buku=$this->loadModel($id);
//		$old=$buku->download;
//		$buku->download= $old+1;
//		$buku->save();
//
//		$this->redirect(array('/site/index'));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$data=new FileBuku('search');
		$data->unsetAttributes();  // clear any default values
		$_GET['FileBuku']['id_buku'] = $id;
		if(isset($_GET['FileBuku']))
			$data->attributes=$_GET['FileBuku'];

			
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Buku']))
		{
			$model->attributes=$_POST['Buku'];
			if($model->save())
			{
				$totalUploadedFiles = count($_FILES['FileBuku']["name"]['files']);
				
				Yii::log("Total files uploaded:" . $totalUploadedFiles);
				for($i=0;$i<count($_FILES['FileBuku']["name"]['files']);$i++)
				{
					if(strlen($_FILES['FileBuku']['name']['files'][$i])>0)
					{
						$model2 = new FileBuku();
						
						$model2['id_buku'] = $model['id'];
						$model2['files'] = $_FILES['FileBuku']["name"]['files'][$i];
						
						Yii::log("Saving file:" . $model2['files']);
						if(!$model2->save())
						{
							print_r($model2->errors);
						}
					}
				}
				
				$data = $_FILES['FileBuku'];
				
				for($i=0;$i<count($data["tmp_name"]["files"]);$i++)
				{
					$tmp_name = $data["tmp_name"]["files"][$i];
					$name = $data["name"]["files"][$i];
					move_uploaded_file($tmp_name, "./file/buku/".$model['id']."_$name");
				}
								
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'model2'=>FileBuku::model()->findAll("id_buku", $id),
			'data'=>$data,
			'newFileBuku'=>new FileBuku(),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			$modelFileBuku = FileBuku::model()->findAll("id_buku=".$model->id);
			foreach($modelFileBuku as $data)
			{
				if(!unlink($_SERVER['DOCUMENT_ROOT'] ."/yii/digilib" . "/file/" . $data->id_buku . "_" . $data->files))
				{
					throw new CException('File cant be deleted.');
				}
			}
			$model->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		print_r(Yii::app()->user->isGuest);die();
		$ajax = Yii::app()->request->getParam("ajax");
		if($ajax == "")
		{
			$ajax = "off";
		}
		
		$criteria=new CDbCriteria(array(
			'condition'=>'status='.Buku::STATUS_PUBLISHED,
			'order'=>'tahun DESC',
		));
		if(isset($_GET['tag']))
			$criteria->addSearchCondition('tags',$_GET['tag']);

		$dataProvider=new CActiveDataProvider('Buku', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['postsPerPage'],
			),
			'criteria'=>$criteria,
		));
		
		if($ajax == "off")
		{
			$this->render('index',array(
				'dataProvider'=>$dataProvider,
			));
		} else {
			$this->renderPartial('index',array(
				'dataProvider'=>$dataProvider,
			));
			
			Yii::app()->end();
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Buku('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Buku']))
			$model->attributes=$_GET['Buku'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionSearch()
	{
		$model=new Buku('search');
		if(isset($_GET['Buku']))
			$model->attributes=$_GET['Buku'];
		$this->render('search',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Suggests tags based on the current user input.
	 * This is called via AJAX when the user is entering the tags input.
	 */
	public function actionSuggestTags()
	{
		if(isset($_GET['q']) && ($keyword=trim($_GET['q']))!=='')
		{
			$tags=Tag::model()->suggestTags($keyword);
			if($tags!==array())
				echo implode("\n",$tags);
		}
	}
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			{
				if(Yii::app()->user->isGuest)
					$condition='status='.Buku::STATUS_PUBLISHED.' OR status='.Buku::STATUS_ARCHIVED;
				else
					$condition='';
				$this->_model=Buku::model()->findByPk($_GET['id'], $condition);
			}
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
		
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='buku-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
