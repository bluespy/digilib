<br/>
<h3 align="center">SELAMAT DATANG DI PERPUSTAKAAN DIGITAL FAKULTAS TEKNIK UNIVERSITAS IBN KHALDUN</h3>
<div id="search" style="border:1px solid; padding:10px;">
        <?php $form=$this->beginWidget('CActiveForm', array(
          'id'=>'search-form',
          'enableAjaxValidation'=>false,
           'htmlOptions'=>array(
               "onSubmit"=>"javascript: $.post('" .
                  Yii::app()->createAbsoluteUrl("site/LoadWordList") .
              "', { wordid : $('#t_search').val(), YII_CSRF_TOKEN : $('input[name=\"YII_CSRF_TOKEN\"]').val() } , function(data) { $('#lyr1').html(data) });return false;"
           )
      )); ?>
          <input type="text" name="t_search" id="t_search" value="Search..." style="color:gray; width: 100%" />
          <?php $this->endWidget(); ?>
      
</div>
<div id="wn"> <!-- scroll area div -->
        <div id="lyr1"> <!-- layer in scroll area (content div) -->
            <?php
              $dataProviderWords = new CActiveDataProvider(Buku::model()->word_only());
            ?>
            <?php echo $this->renderPartial('/site/_wordListContainer', array('dataProviderWords'=>$dataProviderWords)); ?>
            
        </div> <!-- end content div (lyr1) -->
</div>  <!-- end wn div -->
