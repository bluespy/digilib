<?php
$this->breadcrumbs=array(
	'Threads'=>array('kategoriThread/index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Thread', 'url'=>array('index')),
	array('label'=>'Create Thread', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('thread-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Threads</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'thread-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
		'name'=>'judul',
		'value'=>'Thread::model()->findByPk($data["id"])->judul',
		),
		//'user_id',
		array(
			'name'=>'kategori_id',
			'header'=>'Kategori',
			'type'=>'raw',
			'value'=>'KategoriThread::model()->findByPk($data->kategori_id)->kategori',
		),
		//'kategori_id',
		'tanggalPost',
		array(
			'header'=>'Total Komentar',
       		'htmlOptions'=>array('style'=>'text-align: center'),
			'value'=>'CommentThread::model()->countByAttributes(array(\'thread_id\'=>$data->id))',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view} {delete}',
		),
	),
//	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'/id/"+$.fn.yiiGridView.getSelection(id);}',
)); ?>
