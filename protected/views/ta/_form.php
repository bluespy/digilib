<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ta-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'npm'); ?>
		<?php echo $form->textField($model,'npm',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'npm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php echo $form->textField($model,'tahun'); ?>
		<?php echo $form->error($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pembimbing1'); ?>
		<?php echo $form->textField($model,'pembimbing1',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'pembimbing1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pembimbing2'); ?>
		<?php echo $form->textField($model,'pembimbing2',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'pembimbing2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'judul'); ?>
		<?php echo $form->textField($model,'judul',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'judul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'abstrak'); ?>
		<?php $this->widget('application.extensions.cleditor.ECLEditor', array(
			'model'=>$model,
			'attribute'=>'abstrak',
			'options'=>array(
				'width'=>'650',
				'height'=>250,
				'useCSS'=>true,
			),
			'value'=>$model->abstrak,));
		?>
		<?php echo $form->error($model,'abstrak'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'jurusan'); ?>
		<?php echo $form->textField($model,'jurusan',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'jurusan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lokasi'); ?>
		<?php echo $form->textField($model,'lokasi',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lokasi'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->