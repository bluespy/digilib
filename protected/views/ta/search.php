<?php
$this->breadcrumbs=array(
	'Skripsi'=>array('index'),
	'Cari',
);

$this->menu=array(
	array('label'=>'List Ta', 'url'=>array('index')),
	array('label'=>'Create Ta', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ta-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Cari Skripsi</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ta-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'judul',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->judul), $data->url)'
		),
		'jurusan',
		/*
		'abstrak',
		
		'lokasi',
		*/
		
	),
)); ?>
