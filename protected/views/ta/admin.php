<?php
$this->breadcrumbs=array(
	'Skripsi'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Ta', 'url'=>array('index')),
	array('label'=>'Create Ta', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ta-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Skripsi</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ta-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama',
		'npm',
		'tahun',
		'pembimbing1',
		'pembimbing2',
		/*
		'judul',
		'abstrak',
		'jurusan',
		'lokasi',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
