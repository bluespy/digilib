<?php
$this->breadcrumbs=array(
	'Skripsi'=>array('index'),
	$model->nama,
);

$this->menu=array(
	array('label'=>'List Ta', 'url'=>array('index')),
	array('label'=>'Create Ta', 'url'=>array('create')),
	array('label'=>'Update Ta', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Ta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ta', 'url'=>array('admin')),
);
?>

<h1>View Skripsi <?php echo $model->nama; ?></h1><br>

<?php $this->renderPartial('_viv', array(
	'data'=>$model,
)); ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'npm',
		'judul',
		'pembimbing1',
		'pembimbing2',
		'jurusan',
		'tahun',
		'lokasi',
	),
)); ?>
