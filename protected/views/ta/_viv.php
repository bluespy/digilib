<div class="post">

	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->judul), $data->url); ?>
	</div>
	
	<div class="content">
		</br><b>Abstrak</b></br></br>
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			
			echo "<p align='justify'>",$data->abstrak,"</p>";
			$this->endWidget();
		?>
		</div>
	
</div>