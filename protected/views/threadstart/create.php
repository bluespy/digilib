<?php
$this->breadcrumbs=array(
	'Threadstarts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Threadstart', 'url'=>array('index')),
	array('label'=>'Manage Threadstart', 'url'=>array('admin')),
);
?>

<h1>Create Threadstart</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>