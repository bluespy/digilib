<?php
$this->breadcrumbs=array(
	'Threadstarts',
);

$this->menu=array(
	array('label'=>'Create Threadstart', 'url'=>array('create')),
	array('label'=>'Manage Threadstart', 'url'=>array('admin')),
);
?>

<h1>Threadstarts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
