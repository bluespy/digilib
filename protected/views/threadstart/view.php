<?php
$this->breadcrumbs=array(
	'Threadstarts'=>array('index'),
	$model->is,
);

$this->menu=array(
	array('label'=>'List Threadstart', 'url'=>array('index')),
	array('label'=>'Create Threadstart', 'url'=>array('create')),
	array('label'=>'Update Threadstart', 'url'=>array('update', 'id'=>$model->is)),
	array('label'=>'Delete Threadstart', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->is),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Threadstart', 'url'=>array('admin')),
);
?>

<h1>View Threadstart #<?php echo $model->is; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'is',
		'nilai',
		'user_id',
		'thread_id',
	),
)); ?>
