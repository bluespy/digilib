<?php
$this->breadcrumbs=array(
	'Threadstarts'=>array('index'),
	$model->is=>array('view','id'=>$model->is),
	'Update',
);

$this->menu=array(
	array('label'=>'List Threadstart', 'url'=>array('index')),
	array('label'=>'Create Threadstart', 'url'=>array('create')),
	array('label'=>'View Threadstart', 'url'=>array('view', 'id'=>$model->is)),
	array('label'=>'Manage Threadstart', 'url'=>array('admin')),
);
?>

<h1>Update Threadstart <?php echo $model->is; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>