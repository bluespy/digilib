<?php
$this->breadcrumbs=array(
	'User Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserAdmin', 'url'=>array('index')),
	array('label'=>'Create UserAdmin', 'url'=>array('create')),
	array('label'=>'View UserAdmin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserAdmin', 'url'=>array('admin')),
);
?>

<h1>Update Data <?php echo $model->username; ?></h1>
</br>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>