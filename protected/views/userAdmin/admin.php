<?php
$this->breadcrumbs=array(
	'User Admins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List UserAdmin', 'url'=>array('index')),
	array('label'=>'Create UserAdmin', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-admin-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage User</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-admin-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'username',
		'email',
		/*
		'deskripsi',
		'id_level',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
