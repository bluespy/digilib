<?php
$this->breadcrumbs=array(
	'User Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserAdmin', 'url'=>array('index')),
	array('label'=>'Manage UserAdmin', 'url'=>array('admin')),
);
?>

<h1>Registrasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>