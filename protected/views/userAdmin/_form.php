<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-admin-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'repeat password'); ?>
		<?php echo $form->passwordField($model,'password2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'password2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<?php if (extension_loaded('gd')): ?>
        <div class="row">
            <?php echo CHtml::activeLabelEx($model, 'verifyCode') ?>
        <div>
        <?php $this->widget('CCaptcha'); ?><br/>
        <?php echo CHtml::activeTextField($model,'verifyCode'); ?>
        </div>
        <div class="hint">Ketik tulisan yang ada pada gambar .</div>
        </div>
	<?php endif; ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->