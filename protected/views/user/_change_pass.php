<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Isian dengan tanda <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'OldPassword'); ?>
		<?php echo $form->passwordField($model,'OldPassword',array('size'=>41,'maxlength'=>41,'value'=>'')); ?>
		<?php echo $form->error($model,'OldPassword'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'NewPassword'); ?>
		<?php echo $form->passwordField($model,'NewPassword',array('size'=>41,'maxlength'=>41,'value'=>'')); ?>
		<?php echo $form->error($model,'NewPassword'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'ConfirmPassword'); ?>
		<?php echo $form->passwordField($model,'ConfirmPassword',array('size'=>41,'maxlength'=>41,'value'=>'')); ?>
		<?php echo $form->error($model,'ConfirmPassword'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Ubah'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
