<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Isian dengan tanda <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'UserName'); ?>
		<?php echo $form->textField($model,'UserName',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'UserName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Password'); ?>
		<?php echo $form->passwordField($model,'Password',array('size'=>41,'maxlength'=>41, 'value'=>'')); ?>
		<?php echo $form->error($model,'Password'); ?>
	</div>
    
	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'UserGroupId'); ?>
		<?php echo $form->dropDownList($model,'UserGroupId', CHtml::listData(UserLevel::model()->findAll(), "Id", "Name") ); ?>
		<?php echo $form->error($model,'UserGroupId'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'IsActive'); ?>
		<?php echo $form->checkBox($model,'IsActive', array("checked"=>$model->IsActive==0?'':'checked', 'value'=>'1') ); ?>
		<?php echo $form->error($model,'IsActive'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->