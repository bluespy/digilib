<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->Id), array('view', 'id'=>$data->Id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UserName')); ?>:</b>
	<?php echo CHtml::encode($data->UserName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Password')); ?>:</b>
	<?php echo CHtml::encode($data->Password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('LastLogged')); ?>:</b>
	<?php echo CHtml::encode($data->LastLogged); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IsActive')); ?>:</b>
	<?php echo CHtml::encode($data->IsActive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RegisterDate')); ?>:</b>
	<?php echo CHtml::encode($data->RegisterDate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('IsConfirmed')); ?>:</b>
	<?php echo CHtml::encode($data->IsConfirmed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ConfirmSeed')); ?>:</b>
	<?php echo CHtml::encode($data->ConfirmSeed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('UserGroupId')); ?>:</b>
	<?php echo CHtml::encode($data->UserGroupId); ?>
	<br />

	*/ ?>

</div>