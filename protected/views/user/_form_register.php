<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Isian dengan tanda <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'UserName'); ?>
		<?php echo $form->textField($model,'UserName',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'UserName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Password'); ?>
		<?php echo $form->passwordField($model,'Password',array('size'=>41,'maxlength'=>41)); ?>
		<?php echo $form->error($model,'Password'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'ConfirmPassword'); ?>
		<?php echo $form->passwordField($model,'ConfirmPassword',array('size'=>41,'maxlength'=>41)); ?>
		<?php echo $form->error($model,'ConfirmPassword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'Email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'UserGroupId'); ?>
		<?php echo $form->hiddenField($model,'UserGroupId', array('value'=> '2') ); ?>
		<?php //echo $form->error($model,'UserGroupId'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->