<!--protected/views/site/upload.php-->
<div class="yiiForm">
<?php echo CHtml::form('', 'post', array('enctype'=>'multipart/form-data')); ?>
 
<?php echo CHtml::errorSummary($form); ?>
 
<div class="simple">
<?php echo CHtml::activeLabel($form,'image'); ?>
<?php echo CHtml::activeFileField($form, 'image'); ?>
<br/>
<?php echo CHtml::submitButton('Upload'); ?>
</div>
 
<?php echo CHtml::endForm(); ?>
 
</div>