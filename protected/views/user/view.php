<?php
$this->breadcrumbs=array(
	$model->UserName,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->Id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->Id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>Profile</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'UserName',
		'Email',
	),
)); ?>


<h1>Threads</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'thread-grid',
	'dataProvider'=>$model2->search(),
	'filter'=>$model2,
	'columns'=>array(
		//'id',
		array(
		'name'=>'judul',
		'value'=>'Thread::model()->findByPk($data["id"])->judul',
		),
		//'user_id',
		array(
			'name'=>'kategori_id',
			'header'=>'Kategori',
			'type'=>'raw',
			'value'=>'KategoriThread::model()->findByPk($data->kategori_id)->kategori',
		),
		//'kategori_id',
		'tanggalPost',
		array(
			'header'=>'Total Komentar',
       		'htmlOptions'=>array('style'=>'text-align: center'),
			'value'=>'CommentThread::model()->countByAttributes(array(\'thread_id\'=>$data->id))',
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'viewButtonUrl'=>'array("thread/view","id"=>$data["id"])',
		),
	),
//	'selectionChanged'=>'function(id){ location.href = "'.$this->createUrl('view').'/id/"+$.fn.yiiGridView.getSelection(id);}',
)); ?>


