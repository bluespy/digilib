<?php
$this->breadcrumbs=array(
	'Comment Threads',
);

$this->menu=array(
	array('label'=>'Create CommentThread', 'url'=>array('create')),
	array('label'=>'Manage CommentThread', 'url'=>array('admin')),
);
?>

<h1>Comment Threads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
