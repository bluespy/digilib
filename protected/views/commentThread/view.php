<?php
$this->breadcrumbs=array(
	'Comment Threads'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CommentThread', 'url'=>array('index')),
	array('label'=>'Create CommentThread', 'url'=>array('create')),
	array('label'=>'Update CommentThread', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CommentThread', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CommentThread', 'url'=>array('admin')),
);
?>

<h1>View CommentThread #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'judul',
		'isi',
		'user_id',
		'thread_id',
		'tanggalPost',
	),
)); ?>
