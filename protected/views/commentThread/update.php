<?php
$this->breadcrumbs=array(
	'Comment Threads'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CommentThread', 'url'=>array('index')),
	array('label'=>'Create CommentThread', 'url'=>array('create')),
	array('label'=>'View CommentThread', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CommentThread', 'url'=>array('admin')),
);
?>

<h1>Update CommentThread <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>