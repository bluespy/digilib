<?php
$this->breadcrumbs=array(
	'Comment Threads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CommentThread', 'url'=>array('index')),
	array('label'=>'Manage CommentThread', 'url'=>array('admin')),
);
?>

<h1>Create CommentThread</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>