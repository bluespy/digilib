<div class="post">
	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
	</div>
	<div class="author">
		dikirim oleh <?php echo $data->author->username . ', tanggal ' . date('j F Y',$data->create_time); ?>
	</div>
	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo $data->content;
			$this->endWidget();
		?>
	</div>
	<div class="nav">
		<b>Tags:</b>
		<?php echo implode(', ', $data->tagLinks); ?>
		<br/>
		<?php echo CHtml::link("Komentar ({$data->commentCount})",$data->url.'#comments'); ?> |
		Terakhir Diperbaharui Tanggal <?php echo date('j F Y',$data->update_time); ?>
	</div>
</div>
