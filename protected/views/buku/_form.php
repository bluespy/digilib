<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'buku-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'judul'); ?>
		<?php echo $form->textField($model,'judul',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'judul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pengarang'); ?>
		<?php echo $form->textField($model,'pengarang',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'pengarang'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'penerbit'); ?>
		<?php echo $form->textField($model,'penerbit',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'penerbit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php echo $form->textField($model,'tahun'); ?>
		<?php echo $form->error($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'isi'); ?>
		<?php $this->widget('application.extensions.cleditor.ECLEditor', array(
			'model'=>$model,
			'attribute'=>'isi',
			'options'=>array(
				'width'=>'650',
				'height'=>250,
				'useCSS'=>true,
			),
			'value'=>$model->isi,));
		?>
		<?php echo $form->error($model,'isi'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			'model'=>$model,
			'attribute'=>'tags',
			'sourceUrl'=>array('suggestTags'),
			//'multiple'=>true,
			'htmlOptions'=>array('size'=>50),
		)); ?>
		<p class="hint">Please separate different tags with commas.</p>
		<?php echo $form->error($model,'tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keyword'); ?>
		<?php echo $form->textField($model,'keyword',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'keyword'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('PostStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
	<?php if(!is_array($model2)) {?>
	<?php echo $form->labelEx($model2,'files'); ?>
	<?php  $this->widget('CMultiFileUpload',
	  array(
		   'model'=>$model2,
		   'attribute' => 'files',
		   'accept'=> 'doc|docx|pdf|txt',
		   'denied'=>'Only doc,docx,pdf and txt are allowed', 
		   'max'=>15,
		   'remove'=>'[x]',
		   'duplicate'=>'Already Selected',
							  
		   )
			);?>
			
			</div>
	<?php echo $form->error($model,'attachments'); ?>
	<?php } else { ?>
	<?php echo $this->renderPartial('/FileBuku/admin', array('model'=>$data,'model2'=>$newFileBuku,'form'=>$form)); ?>
	<?php } ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->