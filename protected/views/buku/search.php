<?php
$this->breadcrumbs=array(
	'Search Book',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('buku-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Cari Buku</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'judul',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->judul), $data->url)'
		),
		array(
			'name'=>'pengarang',
			'value'=>'$data->pengarang'
		),
		array(
			'name'=>'penerbit',
			'value'=>'$data->penerbit'
		),
	),
)); ?>

