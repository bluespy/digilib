<?php
$this->breadcrumbs=array(
	'buku'=>array('index'),
	$model->judul,
);

$this->menu=array(
	array('label'=>'List Buku', 'url'=>array('index')),
	array('label'=>'Create Buku', 'url'=>array('create')),
	array('label'=>'Update Buku', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Buku', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Buku', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_viv', array(
	'data'=>$model,
)); ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'judul',
		'pengarang',
		'penerbit',
		'tahun',
		'tanggal',
		
	),
)); 

	$this->renderPartial('/fileBuku/index',array(
		'dataProvider'=>$model2,
	));
	
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$dataProvider2,
	'emptyText'=>'No similar subject found !',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'name'=>'10 Similar Document',
			'type'=>'raw',
			'value'=>'Chtml::link($data["judul"],array("buku/view","id"=>$data["id"]))',
		),	
	),
));
?>
