<?php
$this->breadcrumbs=array(
	'Manage Books',
);
?>
<h1>Manage Books</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'judul',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->judul), $data->url)'
		),
		array(
			'name'=>'pengarang',
			'value'=>'$data->pengarang'
		),
		array(
			'name'=>'status',
			'value'=>'Lookup::item("PostStatus",$data->status)',
			'filter'=>Lookup::items('PostStatus'),
		),
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

