<?php
$this->breadcrumbs=array(
	'Bukus'=>array('index'),
	$model->judul=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Buku', 'url'=>array('index')),
	array('label'=>'Create Buku', 'url'=>array('create')),
	array('label'=>'View Buku', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Buku', 'url'=>array('admin')),
);
?>

<h1>Update <?php echo $model->judul; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'model2'=>$model2, 'data'=>$data, 'newFileBuku'=>$newFileBuku)); ?>