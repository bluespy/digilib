<div class="post">

	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->judul), $data->url); ?>
	</div>
	
	<div class="content">
		<b>Kategori:</b>
		<?php echo implode(', ', $data->tagLinks); ?>
	</div>
	
	<div class="content">
		<b>Keyword:</b>
		<?php echo $data->keyword; ?>
	</div>
	<div class="content">
		<br/><b>Description</b><br/><br/>
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			
			echo "<p align='justify'>",$data->isi,"</p>";
			$this->endWidget();
		?>
	</div>
	
</div>