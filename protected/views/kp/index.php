<?php
$this->breadcrumbs=array(
	'Kerja Praktek',
);

$this->menu=array(
	array('label'=>'Create Kp', 'url'=>array('create')),
	array('label'=>'Manage Kp', 'url'=>array('admin')),
);
?>

<h1>Kerja Praktek</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
