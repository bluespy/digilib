<?php
$this->breadcrumbs=array(
	'Kps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Kp', 'url'=>array('index')),
	array('label'=>'Manage Kp', 'url'=>array('admin')),
);
?>

<h1>Create Kp</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>