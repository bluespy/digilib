<?php
$this->breadcrumbs=array(
	'Laporan Kerja Praktek'=>array('index'),
	$model->nama,
);

$this->menu=array(
	array('label'=>'List Kp', 'url'=>array('index')),
	array('label'=>'Create Kp', 'url'=>array('create')),
	array('label'=>'Update Kp', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Kp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Kp', 'url'=>array('admin')),
);
?>

<h1>Laporan Kerja Praktek</h1><br>

<?php $this->renderPartial('_viv', array(
	'data'=>$model,
)); ?>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'judul',
		'nama',
		'npm',
		'pembimbing',
		'pembimbing_lapangan',
		'jurusan',
		'tahun',
		'lokasi_kp',
		'lokasi',
	),
)); 
?>
