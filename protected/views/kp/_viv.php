<div class="post">

	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->judul), $data->url); ?>
	</div>
	
	<div class="content">
		</br><b>Description</b></br></br>
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			
			echo "<p align='justify'>",$data->deskripsi,"</p>";
			$this->endWidget();
		?>
	</div>
	
</div>