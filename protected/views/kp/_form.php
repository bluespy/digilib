<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kp-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'npm'); ?>
		<?php echo $form->textField($model,'npm',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'npm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php echo $form->textField($model,'tahun'); ?>
		<?php echo $form->error($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pembimbing'); ?>
		<?php echo $form->textField($model,'pembimbing',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'pembimbing'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pembimbing_lapangan'); ?>
		<?php echo $form->textField($model,'pembimbing_lapangan',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'pembimbing_lapangan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'judul'); ?>
		<?php echo $form->textField($model,'judul',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'judul'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'deskripsi'); ?>
		<?php $this->widget('application.extensions.cleditor.ECLEditor', array(
			'model'=>$model,
			'attribute'=>'deskripsi',
			'options'=>array(
				'width'=>'650',
				'height'=>250,
				'useCSS'=>true,
			),
			'value'=>$model->deskripsi,));
		?>
		<?php echo $form->error($model,'deskripsi'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'jurusan'); ?>
		<?php echo $form->textField($model,'jurusan',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'jurusan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lokasi_kp'); ?>
		<?php echo $form->textField($model,'lokasi_kp',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lokasi_kp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lokasi'); ?>
		<?php echo $form->textField($model,'lokasi',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'lokasi'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->