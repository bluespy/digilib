<?php
$this->breadcrumbs=array(
	'Kps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Kp', 'url'=>array('index')),
	array('label'=>'Create Kp', 'url'=>array('create')),
	array('label'=>'View Kp', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Kp', 'url'=>array('admin')),
);
?>

<h1>Update Kp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>