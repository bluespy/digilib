<?php
$this->breadcrumbs=array(
	'Laporan Kerja Praktek'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Kp', 'url'=>array('index')),
	array('label'=>'Create Kp', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kp-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage KP</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kp-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'nama',
		'npm',
		'tahun',
		'pembimbing',
		'pembimbing_lapangan',
		/*
		'judul',
		'deskripsi',
		'jurusan',
		'lokasi_kp',
		'lokasi',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
