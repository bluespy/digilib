<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'file-jurnal-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_jurnal'); ?>
		<?php echo $form->textField($model,'id_jurnal'); ?>
		<?php echo $form->error($model,'id_jurnal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'files'); ?>
		<?php echo $form->textField($model,'files',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'files'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->