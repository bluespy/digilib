<?php
$this->breadcrumbs=array(
	'File Jurnals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FileJurnal', 'url'=>array('index')),
	array('label'=>'Create FileJurnal', 'url'=>array('create')),
	array('label'=>'View FileJurnal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FileJurnal', 'url'=>array('admin')),
);
?>

<h1>Update FileJurnal <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>