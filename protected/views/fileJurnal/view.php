<?php
$this->breadcrumbs=array(
	'File Jurnals'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List FileJurnal', 'url'=>array('index')),
	array('label'=>'Create FileJurnal', 'url'=>array('create')),
	array('label'=>'Update FileJurnal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FileJurnal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FileJurnal', 'url'=>array('admin')),
);
?>

<h1>View FileJurnal #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_jurnal',
		'files',
	),
)); ?>
