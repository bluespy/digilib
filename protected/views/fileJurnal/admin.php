<?php  $this->widget('CMultiFileUpload',
	  array(
		   'model'=>$model2,
		   'attribute' => 'files',
		   'accept'=> 'doc|docx|pdf|txt',
		   'denied'=>'Only doc,docx,pdf and txt are allowed', 
		   'max'=>4,
		   'remove'=>'[x]',
		   'duplicate'=>'Already Selected',
							  
		   )
			);?>
	<div class="hint">You can upload up to four attachments. </div>
<?php echo $form->error($model,'attachments'); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'file-jurnal-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'files',
		array(
			'class'=>'CButtonColumn',
			'template' => '{delete}',
			'deleteButtonUrl'=>'Yii::app()->createUrl("filejurnal/delete", array("id"=>$data->id))',
		),
	),
)); ?>
