<?php
$this->breadcrumbs=array(
	'File Jurnals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FileJurnal', 'url'=>array('index')),
	array('label'=>'Manage FileJurnal', 'url'=>array('admin')),
);
?>

<h1>Create FileJurnal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>