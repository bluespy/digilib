<?php
$this->breadcrumbs=array(
	'Reputations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Reputation', 'url'=>array('index')),
	array('label'=>'Manage Reputation', 'url'=>array('admin')),
);
?>

<h1>Create Reputation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>