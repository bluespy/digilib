<?php
$this->breadcrumbs=array(
	'Kategori Threads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List KategoriThread', 'url'=>array('index')),
	array('label'=>'Manage KategoriThread', 'url'=>array('admin')),
);
?>

<h1>Create KategoriThread</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>