<?php
$this->breadcrumbs=array(
	'Kategori Threads'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KategoriThread', 'url'=>array('index')),
	array('label'=>'Create KategoriThread', 'url'=>array('create')),
	array('label'=>'View KategoriThread', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage KategoriThread', 'url'=>array('admin')),
);
?>

<h1>Update KategoriThread <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>