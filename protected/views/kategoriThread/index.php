<?php
$this->breadcrumbs=array(
	'Kategori Forum',
);

$this->menu=array(
	array('label'=>'Create KategoriThread', 'url'=>array('create')),
	array('label'=>'Manage KategoriThread', 'url'=>array('admin')),
);
?>

<h1>Kategori Diskusi</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'summaryText'=>'',
	
)); ?>



 <table>
  <tr>
  
  	<th>
 <?php 
  
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$dataProvider3,
	'emptyText'=>'Belum ada thread pada kategori ini',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'name'=>'Most Thread',
			'type'=>'raw',
			'headerHtmlOptions'=>array('style'=>'text-align: center','colspan'=>'2'),
			'htmlOptions'=>array('width'=>'190px'),
			'value'=>'Chtml::link(User::model()->findByPk($data["user_id"])->UserName,
				array("user/view","id"=>$data["user_id"]))',
		),	
		array(
			'headerHtmlOptions'=>array('style'=>'display:none'),
			'htmlOptions'=>array('style'=>'text-align: center','width'=>'70px'),
			'type'=>'raw',
			'value'=>'$data["count(id)"]',
		),	
	),
));
?>
  </th>
  <th>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$dataProvider4,
	'emptyText'=>'Belum ada thread pada kategori ini',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'type'=>'raw',
			'headerHtmlOptions'=>array('style'=>'display:none'),
			'htmlOptions'=>array('width'=>'190px'),
			'value'=>'Chtml::link(User::model()->findByPk($data["user_id"])->UserName,
				array("user/view","id"=>$data["user_id"]))',
		),	
		array(
			'name'=>'Most Comment',
			'headerHtmlOptions'=>array('style'=>'text-align: center','colspan'=>'2'),
			'htmlOptions'=>array('style'=>'text-align: center','width'=>'70px'),
			'type'=>'raw',
			'value'=>'$data["count(id)"]',
		),	
	),
));
?>
  </th>
</tr>

  <tr>
    <th>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$dataProvider5,
	'emptyText'=>'Belum ada thread pada kategori ini',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'name'=>'Top Thread',
			'type'=>'raw',
			'value'=>'Chtml::link(Thread::model()->findByPk($data["thread_id"])->judul."   (".$data["count(thread_id)"].")",
				array("thread/view","id"=>$data["thread_id"]))',
		),	
	),
));
		?>    
    </th>
    <th>
	<?php 
			$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$dataProvider2,
	'emptyText'=>'Belum ada thread pada kategori ini',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'name'=>'Last Thread',
			'type'=>'raw',
			'value'=>'Chtml::link($data["judul"],array("thread/view","id"=>$data["id"]))',
		),	
	),
));
?>    
    </th>
  </tr>
</table>

