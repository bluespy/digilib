<?php
$this->breadcrumbs=array(
	'Kategori Forum'=>array('index'),
	$model->kategori,
);

$this->menu=array(
	array('label'=>'List KategoriThread', 'url'=>array('index')),
	array('label'=>'Create KategoriThread', 'url'=>array('create')),
	array('label'=>'Update KategoriThread', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KategoriThread', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KategoriThread', 'url'=>array('admin')),
);
?>


<?php $form=$this->beginWidget('CActiveForm', array(
)); ?>

<?php echo Chtml::link('Buat Thread Baru',array('thread/create','id'=>$model->id),array('class'=>'btn success')) ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'thread-grid',
	'dataProvider'=>$allt->search2(),
	//'filter'=>$allt,
	'emptyText'=>'Belum ada thread pada kategori ini',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'name'=>'Judul',
			'type'=>'raw',
			'value'=>'Chtml::link($data->judul,array(\'thread/view\',\'id\'=>$data->id))',
		),
		
		array(
			'name'=>'By',
			'type'=>'raw',
       		'htmlOptions'=>array('style'=>'text-align: center'),
			'value'=>'Chtml::link(UserAdmin::model()->findByPk($data->user_id)->username,array("userAdmin/view","id"=>$data->user_id))',
		),
		array(
			'name'=>'Total Komentar',
       		'htmlOptions'=>array('style'=>'text-align: center'),
			'value'=>'CommentThread::model()->countByAttributes(array(\'thread_id\'=>$data->id))',
		),
	),
)); ?>

<?php $this->endWidget(); ?>