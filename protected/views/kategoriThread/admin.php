<?php 
		
$this->breadcrumbs=array(
	'Kategori Threads'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List KategoriThread', 'url'=>array('index')),
	array('label'=>'Create KategoriThread', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kategori-thread-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Kategori Threads</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'kategori-thread-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'kategori',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
