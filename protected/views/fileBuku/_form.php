<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'file-buku-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_buku'); ?>
		<?php echo $form->textField($model,'id_buku'); ?>
		<?php echo $form->error($model,'id_buku'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'files'); ?>
		<?php echo $form->textField($model,'files',array('size'=>60,'maxlength'=>137)); ?>
		<?php echo $form->error($model,'files'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->