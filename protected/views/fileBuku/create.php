<?php
$this->breadcrumbs=array(
	'File Bukus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FileBuku', 'url'=>array('index')),
	array('label'=>'Manage FileBuku', 'url'=>array('admin')),
);
?>

<h1>Create FileBuku</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>