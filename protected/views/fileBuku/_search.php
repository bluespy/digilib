<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_buku'); ?>
		<?php echo $form->textField($model,'id_buku'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'files'); ?>
		<?php echo $form->textField($model,'files',array('size'=>60,'maxlength'=>137)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->