<?php
$this->breadcrumbs=array(
	'File Bukus'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FileBuku', 'url'=>array('index')),
	array('label'=>'Create FileBuku', 'url'=>array('create')),
	array('label'=>'View FileBuku', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FileBuku', 'url'=>array('admin')),
);
?>

<h1>Update FileBuku <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>