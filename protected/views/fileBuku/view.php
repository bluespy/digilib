<?php

$this->menu=array(
	array('label'=>'List FileBuku', 'url'=>array('index')),
	array('label'=>'Create FileBuku', 'url'=>array('create')),
	array('label'=>'Update FileBuku', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FileBuku', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FileBuku', 'url'=>array('admin')),
);
?>

<h1>View FileBuku #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'files',
	),
)); ?>
