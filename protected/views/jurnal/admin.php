<?php
$this->breadcrumbs=array(
	'Jurnals'=>array('index'),
	'Manage',
);
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jurnal-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'judul',
		'penulis',
		'kategori',
		'waktu_buat',
		'waktu_edit',
		/*
		'download',
		*/
		array(
			'name'=>'status',
			'value'=>'Lookup::item("PostStatus",$data->status)',
			'filter'=>Lookup::items('PostStatus'),
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
