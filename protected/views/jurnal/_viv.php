<div class="post">

	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->judul), $data->url); ?>
	</div>
	
	<div class="content">
		<b>Kategori:</b>
		<?php echo $data->kategori,' ';  echo "<b>, </b>", $data->waktu_buat;?>
	</div>
	
	
	<div class="content">
		<br><b>Description</b><br><br>
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			
			echo "<p align='justify'>",$data->abstrak,"</p>";
			$this->endWidget();
		?>
	</div>
	
</div>