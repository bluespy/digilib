<?php
$this->breadcrumbs=array(
	'Jurnal'=>array('index'),
	$model->judul,
);

$this->menu=array(
	array('label'=>'List Jurnal', 'url'=>array('index')),
	array('label'=>'Create Jurnal', 'url'=>array('create')),
	array('label'=>'Update Jurnal', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Jurnal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Jurnal', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_viv', array(
	'data'=>$model,
)); 
?>

<?php 
$this->renderPartial('/fileJurnal/index',array(
	'dataProvider'=>$model2,
));

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$dataProvider2,
	'emptyText'=>'No similar subject found !',
	'summaryText'=>'',
	'columns'=>array(
		array(
			'name'=>'10 Similar Document',
			'type'=>'raw',
			'value'=>'Chtml::link($data["judul"],array("jurnal/view","id"=>$data["id"]))',
		),	
	),
));
?>