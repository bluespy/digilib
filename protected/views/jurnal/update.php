<?php
$this->breadcrumbs=array(
	'Jurnals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Jurnal', 'url'=>array('index')),
	array('label'=>'Create Jurnal', 'url'=>array('create')),
	array('label'=>'View Jurnal', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Jurnal', 'url'=>array('admin')),
);
?>

<h1>Update Jurnal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'model2'=>$model2, 'data'=>$data, 'newFileJurnal'=>$newFileJurnal)); ?>