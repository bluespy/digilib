<?php
$this->breadcrumbs=array(
	'Jurnal'=>array('index'),
	'Cari',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('jurnal-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Cari Jurnal</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jurnal-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'judul',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->judul), $data->url)'
		),
		'kategori',
	),
)); ?>
