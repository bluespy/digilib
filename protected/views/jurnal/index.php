<?php
$this->breadcrumbs=array(
	'Jurnal',
);

$this->menu=array(
	array('label'=>'Create Kp', 'url'=>array('create')),
	array('label'=>'Manage Kp', 'url'=>array('admin')),
);
?>

<h1>Jurnal</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
)); ?>
