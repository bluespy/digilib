<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'jurnal-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'judul'); ?>
		<?php echo $form->textField($model,'judul',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'judul'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'penulis'); ?>
		<?php echo $form->textField($model,'penulis',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'penulis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'abstrak'); ?>
		<?php $this->widget('application.extensions.cleditor.ECLEditor', array(
			'model'=>$model,
			'attribute'=>'abstrak',
			'options'=>array(
				'width'=>'650',
				'height'=>250,
				'useCSS'=>true,
			),
			'value'=>$model->abstrak,));
		?>
		<?php echo $form->error($model,'abstrak'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'kategori'); ?>
		<?php echo $form->textField($model,'kategori',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'kategori'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('PostStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	
	<div class="row">
	<?php if(!is_array($model2)) {?>
	<?php echo $form->labelEx($model2,'files'); ?>
	<?php  $this->widget('CMultiFileUpload',
	  array(
		   'model'=>$model2,
		   'attribute' => 'files',
		   'accept'=> 'doc|docx|pdf|txt',
		   'denied'=>'Only doc,docx,pdf and txt are allowed', 
		   'max'=>4,
		   'remove'=>'[x]',
		   'duplicate'=>'Already Selected',
							  
		   )
			);?>
			<div class="hint">You can upload up to four attachments. </div>
			</div>
	
	
	<?php echo $form->error($model,'attachments'); ?>
	<?php } else { ?>
	<?php echo $this->renderPartial('/FileJurnal/admin', array('model'=>$data,'model2'=>$newFileJurnal,'form'=>$form)); ?>
	<?php } ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->