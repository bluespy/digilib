<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/jscripts/jquery.js"></script>
	<title>DIGILIB-FT UIKA</title>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/jscripts/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/jscripts/tiny_mce/menu.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/jscripts/menu.js"></script>
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/jscripts/general.js" type="text/javascript"></script>
	
</head>

<body>
<div id="header">
	<div id="red">
	  <div class="menu">
			<?php $this->widget('application.extensions.menu.SMenu',
				array(
				"menu"=>array(
					array("url"=>array("route"=>"site/login"),"label"=>"Masuk",'visible'=>Yii::app()->user->isGuest,
						  ),
					array("url"=>array("route"=>"user/register"),"label"=>"Registrasi",'visible'=>Yii::app()->user->isGuest,
						  ),
					array("url"=>array("route"=>"site/logout"),"label"=>"Keluar",'visible'=>!Yii::app()->user->isGuest,
						  ),
					array("url"=>array(),
							   "label"=>''.Yii::app()->user->name.'',
							   'visible'=>!Yii::app()->user->isGuest,
						  array("url"=>array(
									   "route"=>"user/view",
									   "params"=>array("id"=>''.Yii::app()->user->id.'')),
									   "label"=>"Profil",),
						  array("url"=>array(
									  "route"=>"user/changepass"),
									  "label"=>"Ubah Password",),
						  )
						  ),
				
				"menuID"=>"myMenu",
				"delay"=>3
				)
				)
			;?>
	  </div>
	  </div>
		<div id="silver">
			<ul><li>
		  <div id="logo"></div>
		  </li><li>
		  <div id="logo2"></div>
		  </li></ul>
		</div>
	  <div id="black"><a href="http://apycom.com/"></a></div>
	  <?php /*?><div id="menu_black">
		<ul class="menu_black">
        <li class="first"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/buku/index"><span>Home</span></a></li>
        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/#"><span>Koleksi</span></a></li>
        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/jurnal/index"><span>Jurnal</span></a></li>
        <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/#" class="parent"><span>Cari</span></a>
            <div>
                <ul>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/buku/search"><span>Buku</span></a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/jurnal/search"><span>Jurnal</span></a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ta/search"><span>Skripsi</span></a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/kp/search"><span>Laporan Kerja Praktek</span></a></li>
                </ul>
            </div>
        </li>
		<li><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/page?view=about"><span>About</span></a></li>
        <li class="last"><a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/contact"><span>Contacts</span></a></li>
    </ul>
</div><?php */?>

<div id="menu_black">
		<?php $this->widget('application.extensions.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('site/index')),
				array('label'=>'Koleksi', 'url'=>array('site/koleksi')),
				array('label'=>'Jurnal', 'url'=>array('jurnal/index')),
				array('label'=>'Forum', 'url'=>array('kategorithread/index')),
				array('label'=>'Cari', 'url'=>array('buku/search'), 'items'=>array(
					array('label'=>'Buku', 'url'=>array('buku/search')),
 	            	array('label'=>'Jurnal', 'url'=>array('jurnal/search')),
 	            	array('label'=>'Laporan Kerja Praktek', 'url'=>array('kp/search')),
 	            	array('label'=>'Skripsi', 'url'=>array('ta/search')),
         		)),
				array('label'=>'Kontak', 'url'=>array('site/contact')),
			),
			'htmlOptions'=>array(
							'class' => 'menu_black',
							'style' => 'padding-left: 70px; padding-right:70px;',
			),
		)); ?>
	</div>
<!-- mainmenu -->
	</div><!-- header -->

<div class="container" id="page">	
	<?php echo $content; ?>
	
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> Digital Library of Engineering Faculty | Developed by <a href="#">Mahmud Suhendar</a><br/>
		
	</div><!-- footer -->

</div><!-- page -->
</body>
</html>