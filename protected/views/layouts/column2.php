<?php $this->beginContent('/layouts/main'); ?>
<div class="container">
	
	<div class="span-6 last">
	
		<div id="sidebar">
			<?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>
			
			<?php $this->widget('Menu'); ?>
			
		</div><!-- sidebar -->
	</div>
	<div class="span-18">
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
		<?php if(Yii::app()->user->hasFlash('info-message')){ ?>

	    <div class="flash-success">
	        <?php echo Yii::app()->user->getFlash('info-message'); ?>
	    </div>
	
	    <?php } ?>
	    <?php if(Yii::app()->user->hasFlash('info-notice')){ ?>
	
	    <div class="flash-notice">
	        <?php echo Yii::app()->user->getFlash('info-notice'); ?>
	    </div>
	
	    <?php } ?>
	      <?php if(Yii::app()->user->hasFlash('info-error')){ ?>
	
	    <div class="flash-error">
	        <?php echo Yii::app()->user->getFlash('info-error'); ?>
	    </div>
		<?php } ?>
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>