<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
//	public function authenticate()
//	{
//		$user=UserAdmin::model()->find('LOWER(username)=?',array(strtolower($this->username)));
//		if($user===null)
//			$this->errorCode=self::ERROR_USERNAME_INVALID;
//		else if(!$user->validatePassword($this->password))
//			$this->errorCode=self::ERROR_PASSWORD_INVALID;
//		else
//		{
//			$this->errorCode=self::ERROR_NONE;
//			$this->setState('id_level', $user->id_level);
//			$this->_id=$user->id;
//			
//		}
//		return $this->errorCode==self::ERROR_NONE;
//	
//	}

public function authenticate() {
    Yii::trace("Logging in with username: " . 
            $this->username . " AND password:" . $this->password . " (" . sha1($this->password) . ")");
    $users = User::model()->find("(Email='" . $this->username . "' OR UserName='". $this->username ."') AND IsActive = 1");
    if ($users !== null) {
      Yii::trace("Email found. Logging in with email as username.");
      if ($users->Password !== sha1($this->password))
      {
        Yii::trace("Invalid password.");
      	$this->errorCode = self::ERROR_PASSWORD_INVALID;
      }
      else
      {
        $this->setState("Name", $this->username);
        $this->setState("Id", $users->Id);
        $this->setState('id_level', $users->UserGroupId);
        $this->errorCode = self::ERROR_NONE;
        $this->_id = $users->Id;
        
        $users->LastLogged = date(Yii::app()->params->dbDateFormat);
        $users->update();
      }
    } else {
      Yii::trace("Email not found in database. Logging in with default username.");
//      if($this->username !== Yii::app()->params->defaultUser)
//        $this->errorCode = self::ERROR_USERNAME_INVALID;
//      elseif(md5($this->password) !== Yii::app()->params->defaultPass)
//        $this->errorCode = self::ERROR_PASSWORD_INVALID;
//      else
//      {
//        $this->setState("Name", $this->username);
//        $this->setState("Id", -1);
//        $this->setState("Level", 0);
//        $this->errorCode = self::ERROR_NONE;
//      }
    }
    
    return $this->errorCode == self::ERROR_NONE;
}
  
	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}

