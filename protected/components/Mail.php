<?php 
class Mail
{
  public static function sendmail($to,$rcpt,$subject,$message, $attachment = null)
  {
      $mail = new PHPMailer();

//      $mail->IsSMTP();                                      
//      $mail->Host = Yii::app()->params->smtpHost;  
//      $mail->SMTPAuth = true;    
//      $mail->Username = Yii::app()->params->smtpUser;  
//      $mail->Password = Yii::app()->params->smtpPass; 
//      $mail->Port = Yii::app()->params->smtpPort;
      
      $mail->IsMail();

      $mail->From = Yii::app()->params->smtpMailSender;
      $mail->FromName = Yii::app()->params->smtpMailAlias;
      if(is_array($to))
      {
        for ($i = 0; $i<sizeof($to);$i++) {
          $mail->AddAddress($to[$i], $rcpt[$i]);
        }
      } else
      {
        $mail->AddAddress($to, $rcpt);
      }
      $mail->AddReplyTo(Yii::app()->params->smtpMailSender, Yii::app()->params->smtpMailAlias);

      if (!is_null($attachment) && $attachment[0] != "") {
          $mail->AddAttachment($attachment[0],$attachment[1]);
      }
      
      $mail->WordWrap = 50;                               
      $mail->IsHTML(true);                                 

      $mail->Subject = $subject;
      
      $mail->Body    = $message;
//      if(is_readable($message))
//      {
//        $mail->Body    = file_get_contents($message);
//      } else {
//        $mail->Body    = $message;
//      }

      if(!$mail->Send())
      {
         return false;
      }
      return true;	
  }
}
?>