<?php

Yii::import('zii.widgets.CPortlet');

class KategoriCloud extends CPortlet
{
	public $title='Kategoris';
	public $maxKategoris=20;

	protected function renderContent()
	{
		$kategoris=Kategori::model()->findKategoriWeights($this->maxkategoris);
		foreach($kategoris as $kategori=>$weight)
		{
			$link=CHtml::link(CHtml::encode($kategori), array('post/index','kategori'=>$kategori));
			echo CHtml::kategori('span', array(
				'class'=>'kategori',
				'style'=>"font-size:{$weight}pt",
			), $link)."\n";
		}
	}
}