<?php
class WebUser extends CWebUser
{
    private $_model;
	public function getStatus(){
    	  $user = $this->loadUser(Yii::app()->user->status);
    	  return $user->status;
  	}
	
	function isAdmin(){
          $user = $this->loadUser(Yii::app()->user->status);
          return intval($user->status) == 'admin';
        }
  
        protected function loadUser($id=null)
        {
           if($this->_model===null)
           {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }
}
?>