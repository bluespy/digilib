<div id="navigation" class="navigation">
<ul>
	<?php 
	function getOnClick($route,$tag="")
		{
			$onclick="javascript: $.get('" .
		      Yii::app()->createAbsoluteUrl($route) .
		              "', { ajax : 'on', tag :'$tag'  } , function(data) { $('#content').html(data) });";
		      
		     return $onclick;
		} 
	
		
	?>	
	<li><?php echo CHtml::link('Teknik Informatika', "#", array( "onClick" => getOnClick("buku/index", "Teknik Informatika"))); ?></li>
	<li><?php echo CHtml::link('Teknik Sipil', "#", array( "onClick" => getOnClick("buku/index", "Teknik Sipil"))); ?></li>
	<li><?php echo CHtml::link('Teknik Mesin', "#", array( "onClick" => getOnClick("buku/index", "Teknik Mesin"))); ?></li>
	<li><?php echo CHtml::link('Teknik Elektro', "#", array( "onClick" => getOnClick("buku/index", "Teknik Elektro"))); ?></li>
	<li><?php echo CHtml::link('Umum', "#", array( "onClick" => getOnClick("buku/index", "Umum"))); ?></li>
	<li><?php echo CHtml::link('Skripsi',"#", array( "onClick" => getOnClick('ta/index'))); ?></li>
	<li><?php echo CHtml::link('Kerja Praktek',"#", array( "onClick" => getOnClick('kp/index'))); ?></li>
	
</ul>
</div>
