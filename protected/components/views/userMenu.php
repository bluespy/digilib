<div id="navigation" class="navigation">
	<ul>
	<?php
		if (Yii::app()->user->id_level==1)
		{ ?>
			<li><?php echo CHtml::link('Create New book',array('buku/create')); ?></li>
			<li><?php echo CHtml::link('Manage books',array('buku/admin')); ?></li>
			<li><?php echo CHtml::link('Create New jurnal',array('jurnal/create')); ?></li>
			<li><?php echo CHtml::link('Manage jurnals',array('jurnal/admin')); ?></li>
			<li><?php echo CHtml::link('Create New Skripsi',array('ta/create')); ?></li>
			<li><?php echo CHtml::link('Manage Skripsi',array('ta/admin')); ?></li>
			<li><?php echo CHtml::link('Create New Laporan KP',array('kp/create')); ?></li>
			<li><?php echo CHtml::link('Manage Laporan KP',array('kp/admin')); ?></li>
			<li><?php echo CHtml::link('Manage User',array('user/admin')); ?></li>
			<li><?php echo CHtml::link('Create Kategori Thread',array('kategoriThread/create')); ?></li>
			<li><?php echo CHtml::link('Manage Kategori Thread',array('kategoriThread/admin')); }?></li>
			<li><?php echo CHtml::link('Manage Thread',array('thread/admin')); ?></li>		
	</ul>
</div>
