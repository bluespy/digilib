<?php

Yii::import('zii.widgets.CPortlet');

class Menu extends CPortlet
{
	
	
	public function init()
	{
		$this->title='Kategori';
		parent::init();
	}

	protected function renderContent()
	{
		$this->render('menu');
	}
}