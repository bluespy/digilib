<?php 
/* ******************************************************************************************* ->

	EFileCloaker v.0.1beta
			
	How to use:
		1.	Download and extract EFileCloaker in your extension directory.
		2.	Modify your configuration file to auto import the extension folder
			example:
					'import'=>array(
						'application.extensions.*',
					),
		
		3.	Call the widget.
			Example:
					$this->widget('application.extensions.EFileCloaker', array(
						'_originalFile'=>CHtml::encode($data->files),
						'_originalUrl'=>'../../file/' .  $data->id_buku . "_" . $data->files,
					));
					
		4. 	Create an action to download files
			Example:
					public function actionDownload()
					{
						$uncloak = new EFileCloaker();
						$uncloak->download();
					}
		
		5. Done
		
	Note:
	Sorry guys, it's still a really rough implementation.
	But I hope it will work just fine. It did with mine at least.
	Will update soon.

<- ************************************************************************************************ */

class EFileCloaker extends CWidget {
  private $_randomId;
  public $_originalFile;
  public $_originalUrl;
  private $_cloakPrefix = "id";
  private $_cloakAction = "download";
  private $_cloakUrl;
  private $_html;
  private $_fileName = null;
  
  public function setAction($Action)
  {
    if(is_string($Action)) {
      $this->_cloakAction = $Action;
    } else {
      throw new CException("Action must be string");
    }
  }
  
  public function setFileName($FileName)
  {
    if(is_string($FileName)) {
      $this->_originalFile = $FileName;
    } else {
      throw new CException("FileName must be string");
    }
  }
  
  public function setOriginalUrl($FileAddress) {
    if(is_string($FileAddress)) {
      $this->_originalUrl = $FileAddress;
    } else {
      throw new CException("FileAddress must be string");
    }
  }
  
  public function setCloakPrefix($Prefix) {
    if(is_string($Prefix)) {
      $this->_cloakPrefix = $Prefix;
    } else {
      throw new CException("Prefix must be string");
    }
  }

  public function run() {
    if(!isset ($this->_originalUrl)) {
      throw new CException("Originalurl is not set or it's empty");
    }
    $this->setOriginalFile();
    $this->cloakUrl();
    $this->createMarkup();
  }
    
  public function download()
  {
  	$cc = new Controller($this->genRandomNumber());
  	$ses = new CHttpSession();
  	$ses->open();
  	
  	$key = $_GET[$this->_cloakPrefix];
  	
  	if($ses->contains($key))
  	{
	  	$this->_originalUrl = $ses[$key];
	  	$this->setOriginalFile(); 
	  	
	  	$tmpOri = str_replace("\\", "/", Yii::app()->basePath) . "/.." . str_replace("../","./",$this->_originalUrl);
//		$tmpOri = str_replace("../","./",$this->_originalUrl);

//		print_r($tmpOri); die();
	  	if( stream_resolve_include_path($tmpOri) !== false)
	  	{
			header('Content-disposition: attachment; filename='.$this->_originalFile);
			readfile($tmpOri);
//	  		copy(str_replace("../","./",$this->_originalUrl), $this->_originalFile);
	  	} else {
	  		throw new CException("File not found.");
	  	}
	  	
	  	header('Content-disposition: attachment; filename='.$this->_originalFile);
		readfile($this->_originalFile);
		
//		unlink($this->_originalFile);
		$ses->remove($key);
  	} else 
  	{
  		$cc->redirect(array("/site/index"));
  	}
  }
  
  private function getFileName()
  {
  	$tmp = explode("/",$this->_originalUrl);
    $tmp = $tmp[count(explode("/", $this->_originalUrl))-1];
    return $tmp;
  }
  
  private function getPath()
  {
    $tmp = explode("/", $this->_originalUrl);
    array_pop($tmp);
    return implode("/",$tmp);
  }
  
  private function setOriginalFile()
  {
    $this->_originalFile = is_null($this->_fileName)?$this->getFileName():$this->_fileName;
  }
  
  private function genRandomNumber()
  {
  	return substr(md5(rand(100,999)), 4,8);
  }
  
  private function cloakUrl()
  {
  	$random = $this->genRandomNumber(); 
  	
  	$ses = new CHttpSession();
  	$ses->open();
  	$ses[$random] = $this->_originalUrl;
  	
  	$this->_cloakUrl = $this->_cloakAction . "?" . $this->_cloakPrefix . "=" . $random; 
  	
  	return  $this->_cloakUrl;
  }

  private function createMarkUp() {
  	$this->_html = "<div><a href='".$this->_cloakUrl."'>" . $this->_originalFile . "</a></div>";

    echo $this->_html;
  }
  
  private function set($param){
    isset($param) ? $param = $param : $param = "";
  }
}
?>
