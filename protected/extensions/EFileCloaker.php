<?php

/* * ****************************************************************************************** ->

  EFileCloaker v.0.7beta

  How to use:
  1.	Download and extract EFileCloaker in your extension directory.
  2.	Modify your configuration file to auto import the extension folder
  example:
  'import'=>array(
  'application.extensions.*',
  ),

  3.	Call the widget.
  Example:
  $this->widget('application.extensions.EFileCloaker', array(
  'OriginalFileName'=>'Demo File',
  'OriginalUrl'=>'../../file/demofile.pdf',
  ));

  4. 	Create an action to download files
  Example:
  public function actionDownload()
  {
  $uncloak = new EFileCloaker();
  $uncloak->download();
  }

  5. Done

 * 
 * History:
 * 11/3/2012  - Refactored variables and function name to be more consistent
 *            - Added support for xsendfile when available in apache
 *              Thanks to Pomstiborius (http://www.yiiframework.com/user/4660/) for pointing it out
 * 13/5/2012  - Added rough support for path url type
 * 14/5/2012  - Implement a template method.
 * 26/5/2012  - Faster implementation.
 * 			  - Better support for xsendfile.
 * 27/6/2012  - Cleaned up the code
 *			  - Add method to retrieve originalUrl before download
 *			  - Modified download method to recieve callback functions before downloading occurs
 * 28/6/2012  = Add method to retrieve calling URL (getCallingURL() )

  <- ************************************************************************************************ */

/**
 * 
 */
class EFileCloaker extends CWidget {
	public $OriginalFileName; // This would store the name to be shown as the file name.
	public $OriginalUrl; // This is the original url to be the source of the download file.
	public $TemplateView = "";
	private $_cloakPrefix = "id";
	private $_cloakAction = "download";
	private $_cloakUrl;
	private $_html;
	private $_fileName = null;
	private $_oldUrlFormat;
	private $_oldShowScriptName;

	public function setAction($Action) {
		if (is_string($Action)) {
			$this->_cloakAction = $Action;
		} else {
			throw new CException("Action must be string");
		}
	}

	public function setFileName($FileName) {
		if (is_string($FileName)) {
			$this->OriginalFileName = $FileName;
		} else {
			throw new CException("FileName must be string");
		}
	}

	public function setOriginalUrl($FileAddress) {
		if (is_string($FileAddress)) {
			$this->OriginalUrl = $FileAddress;
		} else {
			throw new CException("FileAddress must be string");
		}
	}

	public function setCloakPrefix($Prefix) {
		if (is_string($Prefix)) {
			$this->_cloakPrefix = $Prefix;
		} else {
			throw new CException("Prefix must be string");
		}
	}

	public function run() {
		if (!isset($this->OriginalUrl)) {
			throw new CException("Originalurl is not set or it's empty");
		}

		$this->_oldUrlFormat = Yii::app()->urlManager->getUrlFormat();
		$this->_oldShowScriptName = Yii::app()->urlManager->showScriptName;

		$this->_setOriginalFile();
		$this->_cloakUrl();
		$this->_createMarkUp();
	}

	public function download($preDownload = "") {
		$key = "";

		$cc = new Controller($this->_genRandomNumber());
		$ses = new CHttpSession();
		$ses->open();

		$key = $this->_getKey();
		if ($ses->contains($key)) {
			$this->OriginalUrl = explode("/", str_replace("\\", "/", Yii::app()->basePath));
			array_pop($this->OriginalUrl);
			$this->OriginalUrl = implode("/", $this->OriginalUrl);
			$this->OriginalUrl .= (substr($ses[$key], 0, 1) == "/" ? "" : "/") . $ses[$key];
			$this->_setOriginalFile();

			if (!file_exists($this->OriginalUrl)) {
				throw new CException("File not found.");
			}
			
			if($preDownload != "") {
				if(is_callable($preDownload)) {
					call_user_func($preDownload, $key, $ses[$key]);
				} else {
					throw new CException("Invalid callback function.");
				}
			}

			$req = new CHttpRequest();
			if (function_exists('apache_get_modules') && in_array('mod_xsendfile', apache_get_modules())) {
				$req->xSendFile($this->OriginalUrl, array("terminate" => false, "saveName" => $this->OriginalFileName));
			} else {
				$req->sendFile($this->OriginalFileName, file_get_contents($this->OriginalUrl), null, false);
			}

			$ses->remove($key);
		} else {
			$cc->redirect(array("/site/index"));
		}

		return null;
	}

	public function getOriginalUrl() {
		$key = "";
		$key = $this->_getKey();
		
		$ses = new CHttpSession();
		$ses->open();
		
		if ($ses->contains($key)) {
			return $ses[$key];
		} else {
			throw new CException("Session key not found.");
		}
		
		return null;
	}
	
	public function getCallingUrl() {
		$key = "";
		$key = "s" . $this->_getKey();
		
		$ses = new CHttpSession();
		$ses->open();
		
		if ($ses->contains($key)) {
			return $ses[$key];
		} else {
			throw new CException("Session key not found.");
		}
		
		return null;
	}

	private function _getKey() {
		$key = "";
		if (!array_key_exists($this->_cloakPrefix, $_GET)) {
			$key = array_keys($_GET);
			$key = array_pop($key);
		} else {
			$key = $_GET[$this->_cloakPrefix];
		}

		if ($key == "") {
			throw new CException("Invalid download url.");
		}
		return $key;
	}

	private function _getFileName() {
		$tmp = explode("/", $this->OriginalUrl);
		$tmp = $tmp[count(explode("/", $this->OriginalUrl)) - 1];
		return $tmp;
	}

	private function _setOriginalFile() {
		if (is_null($this->OriginalFileName) | $this->OriginalFileName == "") {
			$this->OriginalFileName = is_null($this->_fileName) ? $this->_getFileName() : $this->_fileName;
		}
	}

	private function _genRandomNumber() {
		return substr(md5(rand(100, 999)), 4, 8);
	}

	private function _cloakUrl() {
		$random = $this->_genRandomNumber();

		$ses = new CHttpSession();
		$ses->open();
		$ses[$random] = $this->OriginalUrl;
		$ses["s" . $random] = Yii::app()->request->hostInfo . Yii::app()->request->getUrl();
		$this->_cloakUrl = CHtml::normalizeUrl(array("/" . Yii::app()->controller->id . "/" . $this->_cloakAction, $this->_cloakPrefix => $random));
		return $this->_cloakUrl;
	}

	private function _createMarkUp() {
		if (strlen($this->TemplateView) > 0) {
			if (stripos($this->TemplateView, "{url}") === false) {
				throw new CException("{url} must be defined in the template.");
			}

			$this->_html = str_replace(array("{filename}", "{url}"), array($this->OriginalFileName, $this->_cloakUrl), $this->TemplateView);
		} else {
			$this->_html = "<div><a href='" . $this->_cloakUrl . "'>" . $this->OriginalFileName . "</a></div>";
		}


		echo $this->_html;
	}

}

?>
