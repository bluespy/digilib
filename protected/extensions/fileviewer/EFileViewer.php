<?php

class EFileViewer extends CWidget {
	/* This is the ABSOLUTE base that to use for all path resolving. This has an effect on ALL paths (including GUI, sounds etc). */
	public $BasePath = "http://localhost/";
	
	public $BaseFilePath = "E:/xampp/htdocs/";
	
	/* This is the url path following the base path, relatively to the root file of the web */
	public $BaseUrl = "DIGILIB/";
	
	/* When set to true, log messages are printed to the JavaScript console (using the console.log() function) */
	public $LogToJsConsole = "true";
	 
	/* Necessary for proper scaling of the content. */
	public $Scale = "noScale";
	 
	/* Necessary for fullscreen mode. */
	public $AllowFullScreen = "true";
	 
	/* This is the background color used for the Flash element. */
	public $Bgcolor = "#333333";
	 
	public $CurrentPage = 1;
	 
	public $LimitPage = 10;
	
	public $FileName = "";
	
	public $ErrorLevel = "ALL";
	
	public $BackLink = "";	 
	 
	private $_Id = "megazine";
	private $_baseUrl = "";
	private $_baseUrl2 = "";
	private $_baseUrlMagzFile = "";
	private $_publishMagazinePath = "assets\\tmp\\";
	private $_publishMagazineFullPath = "assets\\tmp\\";
	private $_publishMagazineFile = "tmp.jpg";
	private $_XmlFile = ".mz3";
	private $_width = 1024;
	private $_height = 768;
	private $_totalPageNumber = 0;
		 
	 
	public function __construct()
	{	
		$this->_Id = $this->_Id . $this->_genRandomNumber();
		$this->_XmlFile = $this->_Id . $this->_XmlFile;
		
		$this->CurrentPage = Yii::app()->getRequest()->getParam("page", $this->CurrentPage);
		
	}	
	private function _executeBatchScript($filePath)
	{
		$tmpFileName = $this->_genRandomNumber() . ".bat";
		$cmd = "convert -density 150 $filePath " . $this->_publishMagazineFullPath . DIRECTORY_SEPARATOR . $this->_publishMagazineFile;

		$fp = fopen($tmpFileName, 'w+');
		fwrite($fp, "@echo off \n");
		fwrite($fp, $cmd);
		fclose($fp);

		exec($tmpFileName);
		unlink($tmpFileName);
	}
	 
	private function _preparePdf()
	{
		$curStart = ($this->CurrentPage * $this->LimitPage) + 1 - $this->LimitPage;
		$curEnd = ($this->CurrentPage * $this->LimitPage);

		$tmpFileName = $this->_genRandomNumber();
		if($this->_totalPageNumber >=$this->LimitPage && $curEnd <= $this->_totalPageNumber)
		{
				ETcPdf::ExtractPagesTo($this->FileName, $tmpFileName . ".pdf", $curStart,$curEnd);
		} else
		{
			ETcPdf::ExtractPagesTo($this->FileName, $tmpFileName . ".pdf", $curStart,$this->_totalPageNumber);
		}
		

		$this->_executeBatchScript($tmpFileName . ".pdf");
		
		$this->_normalizePdfFileName();

		unlink($tmpFileName . ".pdf");
	}
	 
	public function run() {
		// Generate and publish scripts and styles
		$this->registerClientScripts();
		
		// Prepare dir for output files
		$this->_createPublishDirectory();
		
		// Split Pdf file
		if($this->FileName == "")
		{
			$e = new Exception("File not found.", 404);
		} else
		{
			$this->_totalPageNumber = ETcPdf::getPageNumbers($this->FileName);
			$this->_preparePdf($this->FileName);
		}
		
		
		// Generate configuration for flash
		$this->_generatePublishXMLFile();
		
		// Call and execute flash
	  	$this->_createStarterScript();
	  	$this->_createFlashRequirement();
	}
	 
	public function registerClientScripts() {
		$resources = dirname(__FILE__).DIRECTORY_SEPARATOR."magazine";
		$this->_baseUrl2 = Yii::app()->assetManager->publish($resources);
	  
		// Get the resources path
		$resources = dirname(__FILE__).DIRECTORY_SEPARATOR."js";

		// publish the files
		$this->_baseUrl = Yii::app()->assetManager->publish($resources);

		//Debug : publish style in every request
		//	    Yii::app()->assetManager->publish($resources.'/'.$this->stylesheet);
		//Yii::app()->assetManager->publish($resources.'/menu.js');
		// register the files
		Yii::app()->clientScript->registerScriptFile($this->_baseUrl.'/swfobject.js');
		Yii::app()->clientScript->registerScriptFile($this->_baseUrl.'/swfaddress.js');
		Yii::app()->clientScript->registerScriptFile($this->_baseUrl.'/megazine.js');
		//Yii::app()->clientScript->registerCssFile($this->_baseUrl.'/'.style.css);
	}
	 
	private function _genRandomNumber()
	{
		return substr(md5(rand(100,999)), 4,8);
	}
	 
	private function set($param){
		isset($param) ? $param = $param : $param = "";
	}
	 
	private function _createFlashRequirement()
	{
		$body ='<div id="'. $this->_Id .'">' .
    			'<h1>you need requires FlashPlayer 9</h1>' .
				'<p><a href="http://get.adobe.com/flashplayer/"><img src="http://www.adobe.com/images/shared/download_buttons/get_adobe_flash_player.png" alt="Get Adobe Flash Player"/></a></p>' .
				'<p>Please try the above link first. If you still encounter problems after installing the Flash Player, try this one:</p>' .
				'<p><a href="http://get.adobe.com/shockwave/"><img src="http://www.adobe.com/images/shared/download_buttons/get_adobe_shockwave_player.png" alt="Get Adobe Shockwave Player"/></a></p>' .
				'</div>';

		echo $body;

	}
	 
	private function _createStarterScript()
	{
		$script = '<script type="text/javascript">' .
		//				'//<![CDATA[' .
				'var flashvars = {' .
				'	basePath: "'. $this->_baseUrl2 . "/" .'",' .
				'	xmlFile: "'. $this->_XmlFile.'",' .
				'	logToJsConsole: "'.$this->LogToJsConsole.'"' .
				'};' .
				'var params = {' .
				'	menu: "false",' .
				'	scale: "'.$this->Scale.'",' .
				'	allowFullscreen: "'.$this->AllowFullScreen.'",' .
				'	allowScriptAccess: "always",' .
				'	bgcolor: "'.$this->Bgcolor.'"' .
				'};' .
				'var attributes = {' .
				'	id: "'.$this->_Id.'"' .
				'};' .
		//				'/* Actually load the Flash. */' .
				'swfobject.embedSWF("'.$this->_baseUrl2.'/preloader-fat.swf", "'.$this->_Id.'", "100%", "100%", "9.0.115", "'.$this->_baseUrl.'/expressInstall.swf", flashvars, params, attributes);' .
		//				'//]]>' .
				'</script>';

		echo $script;
	}

	private function _generatePublishXMLFile()
	{	
		$tmpFileName =  $this->BaseFilePath . $this->_baseUrl2 . "/" . $this->_Id . ".mz3";

		$fp = fopen($tmpFileName, 'w+');
		
		fwrite($fp, ' <?xml version="1.0" encoding="utf-8"?>  ');
		fwrite($fp, ' <book plugins="console, keyboardnavigation, navigationbar, links,print, backgroundsounds, pdflinks, anchors,options, batchpages,gallery, swfaddress"  ');
		fwrite($fp, ' pagewidth="'.$this->_width.'"   ');
		fwrite($fp, ' pageheight="'.$this->_height.'"  ');
		fwrite($fp, ' errorlevel="'. $this->ErrorLevel.'"  ');
		fwrite($fp, ' thumbloadtext="please wait"  ');
		fwrite($fp, ' thumbloadtextsize="12"  ');
		fwrite($fp, ' zoomminscale="0.25"  ');
		fwrite($fp, ' zoommaxscale="4.0"  ');
		fwrite($fp, ' zoomsnap="0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 3.0, 4.0"  ');
		fwrite($fp, ' zoomsteps="2"  ');
		fwrite($fp, ' dragrange="30"  ');
		fwrite($fp, ' maxloaded="h"  ');
		fwrite($fp, ' qualitycontrol="true"  ');
		fwrite($fp, ' thumbscale="0.25"  ');
		fwrite($fp, ' elementsfadein="false"  ');
		fwrite($fp, ' cornerhint="false"  ');
		fwrite($fp, ' pagethickness="0.2"  ');
		fwrite($fp, ' shadows="0.3"  ');
		fwrite($fp, ' startpage="1"  ');
		fwrite($fp, ' centercovers="true"  ');
		fwrite($fp, ' pageoffset="0">  ');
		fwrite($fp, ' <foreground>  ');
		fwrite($fp, ' <box background="image(gui/engine/backHome.png)" width="55" height="55" anchors="pw-70,12" url="'. $this->BackLink.'" target="_self"/> ');
		
		if($this->CurrentPage > 1)
		{
			$url = array("/".Yii::app()->controller->id . "/" . Yii::app()->controller->action->id, "page"=>$this->CurrentPage - 1);
			$url = array_merge($url, array_diff_key($_GET, array("page"=>$this->CurrentPage)));
			$url = CHtml::normalizeUrl($url);
			
			fwrite($fp, ' <box title="Previous Reading" background="image(gui/engine/Backward.png)" width="55" height="55" anchors="15,12" url="'. "http://" . $_SERVER['SERVER_NAME'] .$url.'" target="_self"/> ');
		}
		
		if($this->_totalPageNumber > ( ($this->LimitPage * $this->CurrentPage) ) )
		{
			$url = array("/".Yii::app()->controller->id . "/" . Yii::app()->controller->action->id, "page"=>$this->CurrentPage + 1);
			$url = array_merge($url, array_diff_key($_GET, array("page"=>$this->CurrentPage)));
			$url = CHtml::normalizeUrl($url);
			
			fwrite($fp, ' <box title="Continue Reading" background="image(gui/engine/Forward.png)" width="55" height="55" anchors="pw-125,12" url="'. "http://" . $_SERVER['SERVER_NAME'] .$url.'" target="_self"/> ');
		}
		//fwrite($fp, ' <box background="image(gui/engine/pdfDownload.png)" width="55" height="55" anchors="pw-125,12" url="http://localhost/magazine2/magazine/magazine.pdf" target="_self"/> ');
		fwrite($fp, ' </foreground>  ');
		fwrite($fp, ' <chapter anchor="main"> ');
		
		$fileList = scandir($this->_publishMagazineFullPath);
		foreach($fileList as $file)
		{
			if(!is_dir($file))
			{
				fwrite($fp, ' <page> ');
				fwrite($fp, ' <img src="'. $this->_publishMagazinePath . $file .'"/> ');
				fwrite($fp, ' </page> ');
			}
		}
		
		fwrite($fp, ' </chapter> ');
		fwrite($fp, ' </book> ');
		
		fclose($fp);
	}
	
	private function _normalizePdfFileName()
	{
		$fileList = scandir($this->_publishMagazineFullPath);
		foreach($fileList as $file)
		{
			if(!is_dir($file))
			{
				$arr = explode("-", $file);
				$arr2 = explode(".", $arr[1]);
				$number = $arr2[0];
				
				if($number < 10)
				{
					$number = "00" . $number;
				} elseif($number < 100)
				{
					$number = "0" . $number;
				}
				
				$newFileName = $arr[0] . "-" . $number . "." . $arr2[1];
				
				rename($this->_publishMagazineFullPath . DIRECTORY_SEPARATOR . $file, $this->_publishMagazineFullPath . DIRECTORY_SEPARATOR . $newFileName);
				
				list($this->_width, $this->_height) = getimagesize($this->_publishMagazineFullPath . DIRECTORY_SEPARATOR . $newFileName);
			}
		}
	}
	
	private function _createPublishDirectory()
	{
		$assetsFolder = explode("/", $this->_baseUrl2);
		$this->_publishMagazinePath = $this->_baseUrl2 . DIRECTORY_SEPARATOR . "image" . DIRECTORY_SEPARATOR . $this->_Id;
		$this->_publishMagazinePath = str_ireplace($this->BaseUrl, "", $this->_publishMagazinePath);
		$this->_publishMagazinePath = str_ireplace("\\", "/", $this->_publishMagazinePath);
		$this->_publishMagazinePath = str_ireplace("/assets", "assets", $this->_publishMagazinePath);
		$this->_publishMagazinePath = $this->_publishMagazinePath . "/";
		if(is_dir($this->_publishMagazinePath))
		{
			$fileList = scandir($this->_publishMagazineFullPath);
			foreach($fileList as $file)
			{
				if(!is_dir($file))
				{
					unlink($this->_publishMagazineFullPath . DIRECTORY_SEPARATOR . $file);
				}
			}
			rmdir($this->_publishMagazinePath);
		}
		mkdir($this->_publishMagazinePath, 777);
		$this->_publishMagazineFullPath = $this->_publishMagazinePath;
		$this->_publishMagazinePath = str_ireplace("assets/" . $assetsFolder[count($assetsFolder)-1] , "", $this->_publishMagazinePath);
		$this->_publishMagazinePath = str_ireplace("\\", "/", $this->_publishMagazinePath);
		$this->_publishMagazinePath = str_ireplace("/image", "image", $this->_publishMagazinePath);
	}
}
?>