<?php

/**
 * This is the model class for table "tbl_user_admin".
 *
 * The followings are the available columns in table 'tbl_user_admin':
 * @property integer $id_user
 * @property string $username
 * @property string $password
 * @property string $enkrip
 * @property string $email
 * @property string $inisial
 * @property string $deskripsi
 * @property integer $id_level
 *
 * The followings are the available model relations:
 * @property LevelAdmin $idLevel
 */
class UserAdmin extends CActiveRecord
{
	public $password2;
	public $oldPassword;
	public $verifyCode;
	/**
	 * Returns the static model of the specified AR class.
	 * @return UserAdmin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user_admin';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email,verifyCode', 'required','on'=>'General'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!extension_loaded('gd'),'on'=>'General'),
			array('id_level', 'numerical', 'integerOnly'=>true,'on'=>'General'),
			array('username, email', 'length', 'max'=>30,'on'=>'General'),
			array('username', 'filter', 'filter'=>'strtolower','on'=>'General'),
            array('username','unique','on'=>'General'),
			array('password, enkrip, password2, oldPassword', 'length', 'max'=>50,'min'=>5,'on'=>'General'),
			array('password', 'compare','compareAttribute'=>'password2','on'=>'General'),
			array('email','email','checkMX'=>false,'on'=>'General'),
			
			array('oldPassword, password, password2', 'required', 'on'=>'ChangePassword'),
			array('oldPassword', 'checkOldPassword','on'=>'ChangePassword'),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, email, id_level', 'safe', 'on'=>'search'),
		);
	}
	
	public function checkOldPassword($attribute,$params)
	{
		if($this->validatePassword($this->password))
		{
			$this->addError('oldPassword','Incorrect password.');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLevel' => array(self::BELONGS_TO, 'LevelAdmin', 'id_level'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'username' => 'Username',
			'password' => 'Password',
			'email' => 'Email',
			'oldPassword' => 'Old Password',
			'password2' => 'Confirm Password',
		
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('enkrip',$this->enkrip,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('id_level',$this->id_level);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	
	
	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->enkrip)===$this->password;
	}
	

	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password,$salt)
	{
		return md5($salt.$password);
	}


	public function beforeSave()
	{
		$isinya=$this->generateSalt();
		$dua=$this->password;
		$this->enkrip=$isinya;
		$this->password=$this->hashPassword($dua,$isinya);
		$this->id_level=2;
		return true;
	}
	
	/**
	 * Generates a salt that can be used to generate a password hash.
	 * @return string the salt
	 */
	protected function generateSalt()
	{
		return uniqid('',true);
	}
}