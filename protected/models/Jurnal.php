<?php

/**
 * This is the model class for table "{{jurnal}}".
 *
 * The followings are the available columns in table '{{jurnal}}':
 * @property integer $id
 * @property string $judul
 * @property string $penulis
 * @property string $abstrak
 * @property string $kategori
 * @property integer $status
 * @property string $waktu_buat
 * @property string $waktu_edit
 * @property integer $download
 */
class Jurnal extends CActiveRecord
{
	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Jurnal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{jurnal}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, penulis, abstrak, kategori, status', 'required'),
			array('status, download', 'numerical', 'integerOnly'=>true),
			array('status', 'in', 'range'=>array(1,2,3)),
			array('judul, penulis, kategori', 'length', 'max'=>128),
			array('waktu_buat, waktu_edit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, judul, penulis, abstrak, kategori, status, waktu_buat, waktu_edit, download', 'safe', 'on'=>'search'),
		);
	}
	
	public function scopes()
    {
      return array(
        'word_only'=>array(
          'select'=>"*",
        ),
      );
    }
    
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Judul',
			'penulis' => 'Penulis',
			'abstrak' => 'Abstrak',
			'kategori' => 'Kategori',
			'status' => 'Status',
			'waktu_buat' => 'Waktu Buat',
			'waktu_edit' => 'Waktu Edit',
			'download' => 'Download',
		);
	}
	
	public function getUrl()
	{
		return Yii::app()->createUrl('jurnal/view', array(
			'id'=>$this->id,
			'judul'=>$this->judul,
		));
	}
	
	public function similiar($tag,$id)
	{
		$sql="SELECT judul, id FROM tbl_jurnal WHERE kategori LIKE '%$tag%' and id<>'$id'";
		$dataProvider4=new CSqlDataProvider($sql,array(
			'keyField' => 'id',
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
		return $dataProvider4;
	}
	
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('Jurnal/index', 'tag'=>$tag));
		return $links;
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('penulis',$this->penulis,true);
		$criteria->compare('abstrak',$this->abstrak,true);
		$criteria->compare('kategori',$this->kategori,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('waktu_buat',$this->waktu_buat,true);
		$criteria->compare('waktu_edit',$this->waktu_edit,true);
		$criteria->compare('download',$this->download);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}