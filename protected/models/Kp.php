<?php

/**
 * This is the model class for table "{{kp}}".
 *
 * The followings are the available columns in table '{{kp}}':
 * @property integer $id
 * @property string $nama
 * @property string $npm
 * @property integer $tahun
 * @property string $pembimbing
 * @property string $pembimbing_lapangan
 * @property string $judul
 * @property string $deskripsi
 * @property string $jurusan
 * @property string $lokasi_kp
 * @property string $lokasi
 */
class Kp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Kp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kp}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, npm, tahun, pembimbing, pembimbing_lapangan, judul, deskripsi, jurusan, lokasi_kp, lokasi', 'required'),
			array('tahun', 'numerical', 'integerOnly'=>true),
			array('nama, pembimbing, pembimbing_lapangan, judul, jurusan, lokasi_kp, lokasi', 'length', 'max'=>128),
			array('npm', 'length', 'max'=>12),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, npm, tahun, pembimbing, pembimbing_lapangan, judul, deskripsi, jurusan, lokasi_kp, lokasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	public function scopes()
    {
      return array(
        'word_only'=>array(
          'select'=>"*",
        ),
      );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'npm' => 'NPM',
			'tahun' => 'Tahun',
			'pembimbing' => 'Pembimbing',
			'pembimbing_lapangan' => 'Pembimbing Lapangan',
			'judul' => 'Judul',
			'deskripsi' => 'Deskripsi',
			'jurusan' => 'Jurusan',
			'lokasi_kp' => 'Lokasi Kerja Praktek',
			'lokasi' => 'Lokasi',
		);
	}
	public function getUrl()
	{
		return Yii::app()->createUrl('kp/view', array(
			'id'=>$this->id,
			'judul'=>$this->judul,
		));
	}
	
//	public function similiar($tag,$id)
//	{
//		$sql="SELECT judul, id FROM tbl_kp WHERE tags LIKE '%$tag%' and id<>'$id'";
//		$dataProvider4=new CSqlDataProvider($sql,array(
//			'keyField' => 'id',
//			'pagination'=>array(
//				'pageSize'=>10,
//			),
//		));
//		return $dataProvider4;
//	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('npm',$this->npm,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('pembimbing',$this->pembimbing,true);
		$criteria->compare('pembimbing_lapangan',$this->pembimbing_lapangan,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('jurusan',$this->jurusan,true);
		$criteria->compare('lokasi_kp',$this->lokasi_kp,true);
		$criteria->compare('lokasi',$this->lokasi,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}
?>