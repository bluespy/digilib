<?php

/**
 * This is the model class for table "{{file_buku}}".
 *
 * The followings are the available columns in table '{{file_buku}}':
 * @property integer $id
 * @property integer $id_buku
 * @property string $files
 */
class FileBuku extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return FileBuku the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{file_buku}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_buku', 'required'),
			array('id_buku', 'numerical', 'integerOnly'=>true),
			array('files', 'length', 'max'=>137),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_buku, files', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_buku' => 'Id Buku',
			'files' => 'Files',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_buku',$this->id_buku);
		$criteria->compare('files',$this->files,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}