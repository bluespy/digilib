<?php

/**
 * This is the model class for table "{{kategori}}".
 *
 * The followings are the available columns in table '{{kategori}}':
 * @property integer $id
 * @property string $nama
 * @property integer $jumlah
 */
class Kategori extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Kategori the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{kategori}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama', 'required'),
			array('jumlah', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, jumlah', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'jumlah' => 'Jumlah',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 public function findKategoriWeights($limit=20)
	{
		$models=$this->findAll(array(
			'order'=>'jumlah DESC',
			'limit'=>$limit,
		));

		$total=0;
		foreach($models as $model)
			$total+=$model->jumlah;

		$kategoris=array();
		if($total>0)
		{
			foreach($models as $model)
				$kategoris[$model->nama]=8+(int)(16*$model->jumlah/($total+10));
			ksort($kategoris);
		}
		return $kategoris;
	}

	/**
	 * Suggests a list of existing kategoris matching the specified keyword.
	 * @param string the keyword to be matched
	 * @param integer maximum number of kategoris to be returned
	 * @return array list of matching kategori names
	 */
	public function suggestKategoris($keyword,$limit=20)
	{
		$kategoris=$this->findAll(array(
			'condition'=>'nama LIKE :keyword',
			'order'=>'jumlah DESC, Nama',
			'limit'=>$limit,
			'params'=>array(
				':keyword'=>'%'.strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
			),
		));
		$names=array();
		foreach($kategoris as $kategori)
			$names[]=$kategori->nama;
		return $names;
	}

	public static function string2array($kategoris)
	{
		return preg_split('/\s*,\s*/',trim($kategoris),-1,PREG_SPLIT_NO_EMPTY);
	}

	public static function array2string($kategoris)
	{
		return implode(', ',$kategoris);
	}

	public function updateJumlah($oldKategoris, $newKategoris)
	{
		$oldKategoris=self::string2array($oldKategoris);
		$newKategoris=self::string2array($newKategoris);
		$this->addKategoris(array_values(array_diff($newKategoris,$oldKategoris)));
		$this->removeKategoris(array_values(array_diff($oldKategoris,$newKategoris)));
	}

	public function addKategoris($kategoris)
	{
		$criteria=new CDbCriteria;
		$criteria->addInCondition('nama',$kategoris);
		$this->updateCounters(array('jumlah'=>1),$criteria);
		foreach($kategoris as $nama)
		{
			if(!$this->exists('nama=:nama',array(':nama'=>$nama)))
			{
				$kategori=new Kategori;
				$kategori->nama=$nama;
				$kategori->jumlah=1;
				$kategori->save();
			}
		}
	}

	public function removeKategoris($kategoris)
	{
		if(empty($kategoris))
			return;
		$criteria=new CDbCriteria;
		$criteria->addInCondition('nama',$kategoris);
		$this->updateCounters(array('jumlah'=>-1),$criteria);
		$this->deleteAll('jumlah<=0');
	}
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jumlah',$this->jumlah);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}