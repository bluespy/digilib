<?php

/**
 * This is the model class for table "{{buku}}".
 *
 * The followings are the available columns in table '{{buku}}':
 * @property integer $id
 * @property string $judul
 * @property string $pengarang
 * @property string $penerbit
 * @property integer $tahun
 * @property string $isi
 * @property string $tags
 * @property string $keyword
 * @property integer $status
 * @property string $tanggal
 * @property integer $download
 * @property integer $view
 */
class Buku extends CActiveRecord
{
	
	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;

	private $_oldTags;
	
	private $tag;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Buku the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{buku}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, isi, tags, keyword, status, tanggal', 'required'),
			array('tahun, status, download, view', 'numerical', 'integerOnly'=>true),
			array('status', 'in', 'range'=>array(1,2,3)),
			array('judul, pengarang, penerbit, tags', 'length', 'max'=>128),
			array('keyword', 'length', 'max'=>300),
			array('tags', 'match', 'pattern'=>'/^[\w\s,]+$/', 'message'=>'Tags can only contain word characters.'),
			array('tags', 'normalizeTags'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, judul, pengarang, penerbit, tahun, isi, tags, keyword, status, tanggal, download, view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Title',
			'pengarang' => 'Authors',
			'penerbit' => 'Publishers',
			'tahun' => 'Year',
			'isi' => 'Description',
			'tags' => 'Tags',
			'keyword' => 'Keyword',
			'status' => 'Status',
			'tanggal' => 'Added',
			'download' => 'Download',
			'view' => 'View',
		);
	}
	
	/**
	 * @return string the URL that shows the detail of the post
	 */
	public function getUrl()
	{
		return Yii::app()->createUrl('buku/view', array(
			'id'=>$this->id,
			'judul'=>$this->judul,
		));
	}
	
	public function similiar($tag,$id)
	{
		$sql="SELECT judul, id FROM tbl_buku WHERE tags LIKE '%$tag%' and id<>'$id'";
		$dataProvider4=new CSqlDataProvider($sql,array(
			'keyField' => 'id',
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
		return $dataProvider4;
	}
	
	public function scopes()
    {
      return array(
        'word_only'=>array(
          'select'=>"*",
        ),
      );
    }
	
	/**
	 * @return array a list of links that point to the post list filtered by every tag of this post
	 */
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('buku/index', 'tag'=>$tag));
		return $links;
	}
	
	/**
	 * Normalizes the user-entered tags.
	 */
	public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}
	
	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
	protected function afterFind()
	{
		
		parent::afterFind();
		$this->_oldTags=$this->tags;
	}

	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				
			}
			return true;
		}
		else
			return false;
	}

	/**
	 * This is invoked after the record is saved.
	 */
	protected function afterSave()
	{
		parent::afterSave();
		Tag::model()->updateFrequency($this->_oldTags, $this->tags);
	}

	/**
	 * This is invoked after the record is deleted.
	 */
	protected function afterDelete()
	{
		parent::afterDelete();
		Tag::model()->updateFrequency($this->tags, '');
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('pengarang',$this->pengarang,true);
		$criteria->compare('penerbit',$this->penerbit,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('isi',$this->isi,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('download',$this->download);
		$criteria->compare('view',$this->view);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}
?>