<?php

/**
 * This is the model class for table "{{ta}}".
 *
 * The followings are the available columns in table '{{ta}}':
 * @property integer $id
 * @property string $nama
 * @property string $npm
 * @property integer $tahun
 * @property string $pembimbing1
 * @property string $pembimbing2
 * @property string $judul
 * @property string $abstrak
 * @property string $jurusan
 * @property string $lokasi
 */
class Ta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Ta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ta}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, npm, tahun, pembimbing1, pembimbing2, judul, abstrak, jurusan, lokasi', 'required'),
			array('tahun', 'numerical', 'integerOnly'=>true),
			array('nama, pembimbing1, pembimbing2, judul, jurusan, lokasi', 'length', 'max'=>128),
			array('npm', 'length', 'max'=>12),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nama, npm, tahun, pembimbing1, pembimbing2, judul, abstrak, jurusan, lokasi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	public function getUrl()
	{
		return Yii::app()->createUrl('ta/view', array(
			'id'=>$this->id,
			'judul'=>$this->judul,
		));
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	
	public function scopes()
    {
      return array(
        'word_only'=>array(
          'select'=>"*",
        ),
      );
    }
	
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'npm' => 'NPM',
			'tahun' => 'Tahun',
			'pembimbing1' => 'Pembimbing Utama',
			'pembimbing2' => 'Pembimbing Pendamping',
			'judul' => 'Judul',
			'abstrak' => 'Abstrak',
			'jurusan' => 'Jurusan',
			'lokasi' => 'Lokasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('npm',$this->npm,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('pembimbing1',$this->pembimbing1,true);
		$criteria->compare('pembimbing2',$this->pembimbing2,true);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('abstrak',$this->abstrak,true);
		$criteria->compare('jurusan',$this->jurusan,true);
		$criteria->compare('lokasi',$this->lokasi,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}