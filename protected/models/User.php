<?php

/**
 * This is the model class for table "t_user".
 *
 * The followings are the available columns in table 't_user':
 * @property string $Id
 * @property string $UserName
 * @property string $Password
 * @property string $Email
 * @property string $LastLogged
 * @property integer $IsActive
 * @property string $RegisterDate
 * @property integer $IsConfirmed
 * @property string $ConfirmSeed
 * @property string $UserGroupId
 *
 * The followings are the available model relations:
 * @property UserLevel $userGroup
 */
class User extends CActiveRecord
{
	public $OldPassword;
    public $NewPassword;
    public $ConfirmPassword;
    public $IsNewPassword = false;
    /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('UserName, Password, Email, UserGroupId', 'required'),
			array('IsActive, IsConfirmed', 'numerical', 'integerOnly'=>true),
            array('Email', 'email'),
            array('Email, UserName', 'unique'),
			array('UserName, Email', 'length', 'max'=>50),
			array('Password,ConfirmPassword', 'length', 'max'=>100),
            array('Password', 'length', 'min'=>6),
			array('ConfirmSeed', 'length', 'max'=>5),
			array('UserGroupId', 'length', 'max'=>10),
			array('LastLogged, RegisterDate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('Id, UserName, Password, Email, LastLogged, IsActive, RegisterDate, IsConfirmed, ConfirmSeed, UserGroupId', 'safe', 'on'=>'search'),
            
            array('ConfirmPassword', 'required', 'on'=>'register'),
            array('Password', 'compare', 'compareAttribute'=>'ConfirmPassword', 'on'=>'register'),
            
            array('OldPassword, NewPassword, ConfirmPassword', 'required', 'on'=>'changepass'),
            array('NewPassword', 'compare', 'compareAttribute'=>'ConfirmPassword', 'on'=>'changepass'),
            array('OldPassword', 'compareHash', 'compareAttribute'=>'Password', 'hash'=>'sha1', 'on'=>'changepass'),
            array('NewPassword', 'length', 'min'=>6, 'on'=>'changepass'),
		);
	}
    
    public function compareHash($attribute,$params)
    {
      if(isset($params['compareAttribute']) && isset($params['hash']))
      {
        $x1 = eval('return ' . $params['hash'] .'($this->' . $attribute . ');');
        $x2 = eval('return ' . '$this->' . $params['compareAttribute'] . ';');
        if($x1 != $x2)
        {
          $this->addError($attribute, $this->getAttributeLabel($attribute) .
                  ' has to match specifically with ' . $this->getAttributeLabel($params['compareAttribute']));
        }
      } else
      {
        throw new CException("Required attributes are not specified.");
      }
      
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'userGroup' => array(self::BELONGS_TO, 'UserLevel', 'UserGroupId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'Id' => 'ID',
			'UserName' => 'Nama Pengguna',
			'Password' => 'Kata Sandi',
			'Email' => 'Email',
			'LastLogged' => 'Masuk terakhir',
			'IsActive' => 'Aktif',
			'RegisterDate' => 'Tanggal Daftar',
			'IsConfirmed' => 'Sudah dikonfirmasi',
			'ConfirmSeed' => 'Confirm Seed',
			'UserGroupId' => 'Group',
            'ConfirmPassword' => 'Konfirmasi Sandi',
            'OldPassword' => 'Sandi Lama',
            'NewPassword' => 'Sandi Baru'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('Id',$this->Id,true);
		$criteria->compare('UserName',$this->UserName,true);
		$criteria->compare('Password',$this->Password,true);
		$criteria->compare('Email',$this->Email,true);
		$criteria->compare('LastLogged',$this->LastLogged,true);
		$criteria->compare('IsActive',$this->IsActive);
		$criteria->compare('RegisterDate',$this->RegisterDate,true);
		$criteria->compare('IsConfirmed',$this->IsConfirmed);
		$criteria->compare('ConfirmSeed',$this->ConfirmSeed,true);
		$criteria->compare('UserGroupId',$this->UserGroupId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
    public static function isAdmin($uid)
    {
      $model = User::model()->findByPk($uid);
      
      if($model !== null)
      {
        if($model->UserGroupId == 1)
          return true;
      }
      
      return false;
    }
    
    public static function isContributor($uid)
    {
      $model = User::model()->findByPk($uid);
      
      if($model !== null)
      {
        if($model->UserGroupId == 3)
          return true;
      }
      
      return false;
    }
    
    public static function isGuest($uid)
    {
      $model = User::model()->findByPk($uid);
      
      if($model !== null)
      {
        if($model->UserGroupId == 4)
          return true;
      }
      
      return false;
    }
    
    public static function isManager($uid)
    {
      $model = User::model()->findByPk($uid);
      
      if($model !== null)
      {
        if($model->UserGroupId == 2)
          return true;
      }
      
      return false;
    }
    
    public function beforeSave()
	{
		if($this->IsNewPassword || $this->isNewRecord)
        {
          $this->Password = sha1($this->Password);
        }
        if($this->IsActive == "")
        {
          $this->IsActive = 0;
        }
        
        if($this->isNewRecord)
        {
          $this->ConfirmSeed = substr(md5(date("d m Y s u")), 1,5) ;
          $this->IsConfirmed = 0;
          $this->LastLogged = null;
          $this->RegisterDate = date(Yii::app()->params['dbDateFormat']);
          if($this->UserGroupId == "")
          {
            $this->UserGroupId = -13;
          }
        }
		return true;
	}
    
    public function afterValidate()
    {
      if(!$this->IsNewPassword)
      {
        $this->clearErrors('Password');
      }
    }
    
    public function getConfirmationCode()
    {
      if($this->isNewRecord)
      {
        return false;
      }
      
      return crypt($this->RegisterDate, $this->ConfirmSeed);
    }
}