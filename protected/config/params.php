<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'Digilib',
	// this is used in error pages
	'adminEmail'=>'digilib@127.0.0.1',
	// number of posts displayed per page
	'postsPerPage'=>10,
	// maximum number of comments that can be displayed in recent comments portlet
	'recentCommentCount'=>10,
	// maximum number of tags that can be displayed in tag cloud portlet
	'tagCloudCount'=>20,
	// whether post comments need to be approved before published
	'commentNeedApproval'=>true,
	// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright &copy; 2011 Mahmud Suhendar.',
	'smtpHost' => '127.0.0.1',
        'smtpUser' => 'digilib@127.0.0.1',
        'smtpPass' => 'digilib',
        'smtpPort' => '25',
        'smtpMailSender' => 'digilib@127.0.0.1',
    'smtpMailAlias'=>'PERPUSTAKAAN DIGITAL',
	'dbDateFormat' => 'Y-m-d\TH:i:s',
        
);
