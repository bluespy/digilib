/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.12-log : Database - mahmud_digilib
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mahmud_digilib` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `mahmud_digilib`;

/*Table structure for table `tbl_admin` */

DROP TABLE IF EXISTS `tbl_admin`;

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_admin` */

insert  into `tbl_admin`(`id`,`username`,`password`,`salt`) values (1,'hedar','2e5c7db760a33498023813489cfadc0b','28b206548469ce62182048fd9cf91760');

/*Table structure for table `tbl_buku` */

DROP TABLE IF EXISTS `tbl_buku`;

CREATE TABLE `tbl_buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pengarang` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `penerbit` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `file` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `id_author` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_buku` */

/*Table structure for table `tbl_file_jurnal` */

DROP TABLE IF EXISTS `tbl_file_jurnal`;

CREATE TABLE `tbl_file_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jurnal` int(11) NOT NULL,
  `files` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_file_jurnal` */

/*Table structure for table `tbl_jurnal` */

DROP TABLE IF EXISTS `tbl_jurnal`;

CREATE TABLE `tbl_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `kategori` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `waktu_buat` datetime DEFAULT NULL,
  `waktu_edit` datetime DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_jurnal` */

/*Table structure for table `tbl_kategori` */

DROP TABLE IF EXISTS `tbl_kategori`;

CREATE TABLE `tbl_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_kategori` */

/*Table structure for table `tbl_kp` */

DROP TABLE IF EXISTS `tbl_kp`;

CREATE TABLE `tbl_kp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `npm` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `pembimbing` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pembimbing_lapangan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi_kp` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_kp` */

/*Table structure for table `tbl_lookup` */

DROP TABLE IF EXISTS `tbl_lookup`;

CREATE TABLE `tbl_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_lookup` */

insert  into `tbl_lookup`(`id`,`name`,`code`,`type`,`position`) values (1,'Draft',1,'PostStatus',1),(2,'Published',2,'PostStatus',2),(3,'Archived',3,'PostStatus',3),(4,'Pending Approval',1,'CommentStatus',1),(5,'Approved',2,'CommentStatus',2);

/*Table structure for table `tbl_ta` */

DROP TABLE IF EXISTS `tbl_ta`;

CREATE TABLE `tbl_ta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `npm` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `pembimbing1` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pembimbing2` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `abstrak` text COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_ta` */

/*Table structure for table `tbl_tag` */

DROP TABLE IF EXISTS `tbl_tag`;

CREATE TABLE `tbl_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_tag` */

insert  into `tbl_tag`(`id`,`name`,`frequency`) values (2,'Teknik Informatika',2),(3,'Teknik Mesin',0),(4,'Teknik Sipil',0),(5,'Teknik Elektro',0),(6,'Tugas Akhir',0),(7,'Kerja Praktek',0);

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username_user` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id_user`,`username_user`,`password`,`salt`,`email`) values (1,'hendar','2e5c7db760a33498023813489cfadc0b','28b206548469ce62182048fd9cf91760','hendaar@gmail.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
