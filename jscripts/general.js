// >1
// Adds dynamic display to search textbox
//

$('document').ready(function() {
  $('#t_search').bind("focus", function(){
    if($('#t_search').val() == "Search...")
      {
        $('#t_search').val("");
        $('#t_search').css("color","black");
      }
  })
});

$('document').ready(function() {
  $('#t_search').bind("blur", function(){
    if($('#t_search').val().length > 0)
      {
        $('#t_search').css("color","black");
      } else {
        $('#t_search').val("Search...");
        $('#t_search').css("color","gray");
      }
  })
})