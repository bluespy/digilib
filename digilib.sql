-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 30, 2011 at 09:16 
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `digilib`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `salt`) VALUES
(1, 'hedar', '2e5c7db760a33498023813489cfadc0b', '28b206548469ce62182048fd9cf91760');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_buku`
--

CREATE TABLE IF NOT EXISTS `tbl_buku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pengarang` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `penerbit` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL,
  `file` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `id_author` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_buku`
--

INSERT INTO `tbl_buku` (`id`, `judul`, `pengarang`, `penerbit`, `tahun`, `isi`, `tags`, `status`, `file`, `download`, `view`, `id_author`) VALUES
(2, 'php manual', 'hendar', 'hendar', 2011, 'Merupakan kumpulan kebijaksanaan & mekanisme pada SO berkenaan dg urutan kerja yg dilakukan sistem komputer\r\nBertugas memutuskan proses yg harus berjalan, kapan, dan selama berapa lama proses itu berjalan.\r\nSasaran utama penjadwalan proses adalah kinerja menurut kriteria yg diajukan\r\nKriteria2 yang digunakan utk mengukur dan optimasi kinerja penjadwalan: adil (fairness), efisiensi, waktu tanggap (response time), turn arround time, throughput\r\nAdil (fairness)\r\nProses2 diberlakukan sama -> mendapat jatah waktu pemroses yg sama & tak ada proses yg tak kebagian layanan pemroses\r\nSasaran: menjamin tiap proses mendapat bagian dari pemroses yg adil\r\nEfisiensi\r\nPemroses dihitung dg berapa perbandingan (rasio) waktu sibuk pemroses.\r\nSasaran: menjaga proses agar tetap dalam keadaan sibuk -> efisiensi maksimum\r\nWaktu Tanggap (response time)\r\nWaktu yg dihabiskan dari saat karakter terakhir dari perintah dimasukkan program sampai hasil pertama yg muncul di layar\r\nSasaran: meminimalkan waktu tanggap\r\nTurn Arrround Time\r\nWaktu yg dihabiskan dari saat program/job mulai masuk ke sistem sampai proses diselesaikan sistem\r\nTA = waktu eksekusi + waktu menunggu\r\nSasaran: meminimalkan turn arround time\r\nThroughput\r\nJumlah kerja yg dapat diselesaikan dalam satu unit waktu\r\nSasaran: memaksimalkan jumlah job yang diproses per satu interval waktu\r\nLebih tinggi angka throughput, lebih banyak kerja yang dilakukan sistem\r\n', 'Teknik Informatika', 2, '', NULL, NULL, 1),
(3, 'nyobain', 'hendar', 'hendar', 2010, 'banyak', 'Teknik Informatika', 2, '', NULL, NULL, 1),
(4, 'yang lainnya', 'hendar', 'hendar', 2010, 'banyak', 'Teknik Informatika', 2, '', NULL, NULL, 1),
(5, 'yii framework', 'hendar', 'hendar', 2010, 'lebih banyak', 'Teknik Informatika', 2, '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_file_jurnal`
--

CREATE TABLE IF NOT EXISTS `tbl_file_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_jurnal` int(11) NOT NULL,
  `files` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_file_jurnal`
--

INSERT INTO `tbl_file_jurnal` (`id`, `id_jurnal`, `files`) VALUES
(1, 1, '9584-chapter-9-iteration-6-adding-user-comments.pdf'),
(2, 1, '9584-chapter-9-iteration-6-adding-user-comments.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jurnal`
--

CREATE TABLE IF NOT EXISTS `tbl_jurnal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL,
  `kategori` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `waktu_buat` datetime DEFAULT NULL,
  `waktu_edit` datetime DEFAULT NULL,
  `download` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_jurnal`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_kategori`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_kp`
--

CREATE TABLE IF NOT EXISTS `tbl_kp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `npm` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `pembimbing` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pembimbing_lapangan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi_kp` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `tbl_kp`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbl_lookup`
--

CREATE TABLE IF NOT EXISTS `tbl_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_lookup`
--

INSERT INTO `tbl_lookup` (`id`, `name`, `code`, `type`, `position`) VALUES
(1, 'Draft', 1, 'PostStatus', 1),
(2, 'Published', 2, 'PostStatus', 2),
(3, 'Archived', 3, 'PostStatus', 3),
(4, 'Pending Approval', 1, 'CommentStatus', 1),
(5, 'Approved', 2, 'CommentStatus', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ta`
--

CREATE TABLE IF NOT EXISTS `tbl_ta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `npm` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tahun` int(4) NOT NULL,
  `pembimbing1` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pembimbing2` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `abstrak` text COLLATE utf8_unicode_ci NOT NULL,
  `jurusan` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_ta`
--

INSERT INTO `tbl_ta` (`id`, `nama`, `npm`, `tahun`, `pembimbing1`, `pembimbing2`, `judul`, `abstrak`, `jurusan`, `lokasi`) VALUES
(1, 'Mahmud Suhendar', '07215410445', 2011, 'Foni agus ', 'Peti', 'RANCANG BANGUN PERPUSTAKAAN DIGITAL BERBASIS WEB MENGGUNAKAN DESIGN PATTERN MVC DI FAKULTAS TEKNIK UNIVERSITAS IBN KHALDUN BOGOR', 'ga jelas', 'Teknik Informatika', '09871');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag`
--

CREATE TABLE IF NOT EXISTS `tbl_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_tag`
--

INSERT INTO `tbl_tag` (`id`, `name`, `frequency`) VALUES
(2, 'Teknik Informatika', 2),
(3, 'Teknik Mesin', 0),
(4, 'Teknik Sipil', 0),
(5, 'Teknik Elektro', 0),
(6, 'Tugas Akhir', 0),
(7, 'Kerja Praktek', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username_user` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username_user`, `password`, `salt`, `email`) VALUES
(1, 'hendar', '2e5c7db760a33498023813489cfadc0b', '28b206548469ce62182048fd9cf91760', 'hendaar@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
